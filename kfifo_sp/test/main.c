#include <stdio.h>
#include <stdint.h>
#include "kfifo_sp.c"

struct rx_t {
    uint8_t rxbuffer[5];
};

DECLARE_KFIFO(myfifo, struct rx_t , 8);

void set(struct rx_t *p, uint8_t val) {
    memset(p, val, sizeof(struct rx_t));
}

void show() {
    printf("======================\n");
    printf("myfifo.kfifo.in     : %d\n", myfifo.kfifo.in);
    printf("myfifo.kfifo.out    : %d\n", myfifo.kfifo.out);
    printf("myfifo.kfifo.mask   : %d\n", myfifo.kfifo.mask);
    printf("myfifo.kfifo.esize  : %d\n", myfifo.kfifo.esize);
}

int main(void)
{
    show();
    INIT_KFIFO(myfifo);
    show();

    struct rx_t rx;
    uint8_t val = 0xAA;
    for (int i = 0; i < 7; i++) {
        set(&rx, val++);
        kfifo_in(&myfifo, &rx, 1);
        show();
    }
    printf("################ out ####################\n");
    for (int i = 0; i < 3; i++) {
        kfifo_out(&myfifo, &rx, 1);
        show();
    }
    printf("################ in ####################\n");
    for (int i = 0; i < 7; i++) {
        set(&rx, val++);
        kfifo_in(&myfifo, &rx, 1);
        show();
    }
    return 0;
}
