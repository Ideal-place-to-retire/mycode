/*
    压缩文本文件：
为此，我们使用霍夫曼编码算法，这是一种贪婪算法，
为文本文件中的每个输入字符分配可变长度的二进制代码。
二进制代码的长度取决于文件中字符的频率。
该算法建议创建一个二叉树，其中文件的所有唯一字符都存储在树的叶节点中。

该算法的工作原理是首先确定文件的所有唯一字符及其频率。
然后将字符和频率添加到最小堆中。
然后，它提取两个最低频率字符，并将它们作为节点添加到虚拟根。
此虚拟根的值是其节点的组合频率，并且此根节点将添加回最小堆。
然后重复该过程，直到最小堆中只剩下一个元素。
这样，就可以为特定文本文件创建霍夫曼树。

构建霍夫曼树的步骤：

1、算法的输入是文本文件中的字符数组。
2、计算文件中每个字符的出现频率。
3、创建结构数组，其中每个元素都包含字符及其频率。
    它们存储在优先级队列（最小堆）中，其中元素使用其频率进行比较。
4、为了构建霍夫曼树，从最小堆中提取了两个频率最小的元素。
5、这两个节点作为左子节点和右子节点添加到树中，该根节点包含的频率等于两个频率之和。
    将较低频率的字符添加到左侧子节点，将较高频率的字符添加到右侧子节点中。
6、然后，根节点将再次添加回优先级队列。
7、从步骤 4 开始重复，直到优先级队列中只剩下一个元素。
8、最后，树的左边缘和右边缘分别编号为 0 和 1。对于每个叶节点，遍历整个树，并将相应的 1 和 0 追加到其代码中，直到遇到叶节点。
9、一旦我们有了文本中每个唯一字符的唯一代码，我们就可以用它们的代码替换文本字符。
    这些代码将以逐位形式存储，这将比文本占用更少的空间





压缩文件结构：

到目前为止，我们已经讨论了可变长度输入代码生成并将其替换为文件的原始字符。
但是，这仅用于压缩文件。更困难的任务是通过将二进制代码解码为其原始值来解压缩文件。

这将需要在我们的压缩文件中添加一些额外的信息，以便在解码过程中使用它。
因此，我们将字符及其相应的代码包含在文件中。在解码过程中，这有助于霍夫曼树的重建。

压缩文件的结构 ：
—————————————————————
|输入文件中的唯一字符数
——————————————————————————
|输入文件中的总字符数
——————————————————————————————
|所有字符及其二进制代码（用于解码）
——————————————————————————————————————
|通过逐个替换输入文件的字符来存储二进制代码
————————————————————————————————————————


解压压缩文件：
1、将打开压缩文件，并检索文件中的唯一字符数和字符总数。
2、然后从文件中读取字符及其二进制代码。我们可以使用它重新创建霍夫曼树。
3、对于每个二进制代码：
    为 0 创建左边缘，为 1 创建右边缘。
    最后，形成一个叶节点，并将字符存储在其中。
    对所有字符和二进制代码重复此操作。因此，霍夫曼树以这种方式重新创建。
4、现在逐位读取剩余的文件，并遍历树中相应的 0/1 位。一旦在树中遇到叶节点，相应的字符就会写入解压缩文件。
5、重复步骤 4，直到完全读取压缩文件。
通过这种方式，我们将输入文件中的所有字符恢复到新解压缩的文件中，而不会造成数据或质量损失。

按照上述步骤，我们可以压缩文本文件，然后克服将文件解压缩为原始内容的更大任务，而不会丢失任何数据。

时间复杂度：O（N * logN），其中N是唯一字符的数量，因为有效的优先级队列数据结构每次插入需要O（logN）时间，具有N个叶子的完整二叉树具有（2 * N – 1）节点。


*/

/*************************************
 * 以下代码均为片段，没有形成完整的实现
 * ****************************************/

// 打开输入输出文件
// Opening input file in read-only mode

int fd1 = open(“sample.txt”, O_RDONLY);

if (fd1 == -1) {
	perror("Open Failed For Input File:\n");
	exit(1);
}

// Creating output file in write mode
int fd2 = open(“sample - compressed.txt”,
			O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
if (fd2 == -1) {
	perror("Open Failed For Output File:\n");
	exit(1);
}



// Structure for tree nodes
struct Node {
	char character;
	int freq;
	struct Node *l, *r;
};

// Structure for min heap
struct Min_Heap {
	int size;
	struct Node** array;
};

// 创建最小堆的 函数
// Function to create min heap
struct Min_Heap* createAndBuildMin_Heap(char arr[],
										int freq[],
										int unique_size)
{
	int i;

	// Initializing heap
	struct Min_Heap* Min_Heap
		= (struct Min_Heap*)malloc(sizeof(struct Min_Heap));
	Min_Heap->size = unique_size;
	Min_Heap->array = (struct Node**)malloc(
		Min_Heap->size * sizeof(struct Node*));

	// Initializing the array of pointers in minheap.
	// Pointers pointing to new nodes of character
	// and their frequency
	for (i = 0; i < unique_size; ++i) {

		// newNode is a function
		// to initialize new node
		Min_Heap->array[i] = newNode(arr[i], freq[i]);
	}

	int n = Min_Heap->size - 1;
	for (i = (n - 1) / 2; i >= 0; --i) {

		// Standard function for Heap creation
		Heapify(Min_Heap, i);
	}

	return Min_Heap;
}



//构建霍夫曼树 的函数
// Function to build Huffman Tree
struct Node* buildHuffmanTree(char arr[], int freq[],
							int unique_size)
{
	struct Node *l, *r, *top;
	while (!isSizeOne(Min_Heap)) {
		l = extractMinFromMin_Heap(Min_Heap);
		r = extractMinFromMin_Heap(Min_Heap);
		top = newNode('$', l->freq + r->freq);
		top->l = l;
		top->r = r;
		insertIntoMin_Heap(Min_Heap, top);
	}
	return extractMinFromMin_Heap(Min_Heap);
}


//将二进制代码打印到压缩文件中的递归函数
// Structure to store codes in compressed file
typedef struct code {
	char k;
	int l;
	int code_arr[16];
	struct code* p;
} code;

// Function to print codes into file
void printCodesIntoFile(int fd2, struct Node* root,
						int t[], int top = 0)
{
	int i;
	if (root->l) {
		t[top] = 0;
		printCodesIntoFile(fd2, root->l, t, top + 1);
	}

	if (root->r) {
		t[top] = 1;
		printCodesIntoFile(fd2, root->r, t, top + 1);
	}

	if (isLeaf(root)) {
		data = (code*)malloc(sizeof(code));
		tree = (Tree*)malloc(sizeof(Tree));
		data->p = NULL;
		data->k = root->character;
		tree->g = root->character;
		write(fd2, &tree->g, sizeof(char));
		for (i = 0; i < top; i++) {
			data->code_arr[i] = t[i];
		}
		tree->len = top;
		write(fd2, &tree->len, sizeof(int));
		tree->dec
			= convertBinaryToDecimal(data->code_arr, top);
		write(fd2, &tree->dec, sizeof(int));
		data->l = top;
		data->p = NULL;
		if (k == 0) {
			front = rear = data;
			k++;
		}
		else {
			rear->p = data;
			rear = rear->p;
		}
	}
}


//使用霍夫曼代码替换字符来压缩文件的函数
// Function to compress file
void compressFile(int fd1, int fd2, unsigned char a)
{
	char n;
	int h = 0, i;

	// Codes are written into file in bit by bit format
	while (read(fd1, &n, sizeof(char)) != 0) {
		rear = front;
		while (rear->k != n && rear->p != NULL) {
			rear = rear->p;
		}
		if (rear->k == n) {
			for (i = 0; i < rear->l; i++) {
				if (h < 7) {
					if (rear->code_arr[i] == 1) {
						a++;
						a = a << 1;
						h++;
					}
					else if (rear->code_arr[i] == 0) {
						a = a << 1;
						h++;
					}
				}
				else if (h == 7) {
					if (rear->code_arr[i] == 1) {
						a++;
						h = 0;
					}
					else {
						h = 0;
					}
					write(fd2, &a, sizeof(char));
					a = 0;
				}
			}
		}
	}
	for (i = 0; i < 7 - h; i++) {
		a = a << 1;
	}
	write(fd2, &a, sizeof(char));
}



//从压缩文件中提取的数据 构建霍夫曼树的函数

typedef struct Tree {
	char g;
	int len;
	int dec;
	struct Tree* f;
	struct Tree* r;
} Tree;

// Function to extract Huffman codes
// from a compressed file
void ExtractCodesFromFile(int fd1)
{
	read(fd1, &t->g, sizeof(char));
	read(fd1, &t->len, sizeof(int));
	read(fd1, &t->dec, sizeof(int));
}

// Function to rebuild the Huffman tree
void ReBuildHuffmanTree(int fd1, int size)
{
	int i = 0, j, k;
	tree = (Tree*)malloc(sizeof(Tree));
	tree_temp = tree;
	tree->f = NULL;
	tree->r = NULL;
	t = (Tree*)malloc(sizeof(Tree));
	t->f = NULL;
	t->r = NULL;
	for (k = 0; k < size; k++) {
		tree_temp = tree;
		ExtractCodesFromFile(fd1);
		int bin[MAX], bin_con[MAX];
		for (i = 0; i < MAX; i++) {
			bin[i] = bin_con[i] = 0;
		}
		convertDecimalToBinary(bin, t->dec, t->len);
		for (i = 0; i < t->len; i++) {
			bin_con[i] = bin[i];
		}

		for (j = 0; j < t->len; j++) {
			if (bin_con[j] == 0) {
				if (tree_temp->f == NULL) {
					tree_temp->f
						= (Tree*)malloc(sizeof(Tree));
				}
				tree_temp = tree_temp->f;
			}
			else if (bin_con[j] == 1) {
				if (tree_temp->r == NULL) {
					tree_temp->r
						= (Tree*)malloc(sizeof(Tree));
				}
				tree_temp = tree_temp->r;
			}
		}
		tree_temp->g = t->g;
		tree_temp->len = t->len;
		tree_temp->dec = t->dec;
		tree_temp->f = NULL;
		tree_temp->r = NULL;
		tree_temp = tree;
	}
}




//解压缩 压缩文件的函数
void decompressFile(int fd1, int fd2, int f)
{
	int inp[8], i, k = 0;
	unsigned char p;
	read(fd1, &p, sizeof(char));
	convertDecimalToBinary(inp, p, 8);
	tree_temp = tree;
	for (i = 0; i < 8 && k < f; i++) {
		if (!isroot(tree_temp)) {
			if (i != 7) {
				if (inp[i] == 0) {
					tree_temp = tree_temp->f;
				}
				if (inp[i] == 1) {
					tree_temp = tree_temp->r;
				}
			}
			else {
				if (inp[i] == 0) {
					tree_temp = tree_temp->f;
				}
				if (inp[i] == 1) {
					tree_temp = tree_temp->r;
				}
				if (read(fd1, &p, sizeof(char)) != 0) {
					convertDecimalToBinary(inp, p, 8);
					i = -1;
				}
				else {
					break;
				}
			}
		}
		else {
			k++;
			write(fd2, &tree_temp->g, sizeof(char));
			tree_temp = tree;
			i--;
		}
	}
}




int main()
{

}















