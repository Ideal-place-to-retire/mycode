#include <stdio.h>
#include <stdlib.h>




// void get_hzk_code(char *fname, unsigned char *c, unsigned char buff[])
// {
//     FILE *HZK;
//     if((HZK = fopen(fname, "rb")) == NULL) {
//         printf("Can't open font file!");
//     }

//     int hv = *c - 0xa1;
//     int lv = *(c+1) - 0xa1;
//     long offset = (long) (94 * hv + lv) * 32;

//     fseek(HZK, offset, SEEK_SET);
//     fread(buff, 32, 1, HZK);
//     fclose(HZK);
// }




unsigned char str[32] = 
{
    0x00,0x20,0x22,0x2C,0x20,0x20,0xE0,0x3F,0x20,0x20,0x20,0x20,0xE0,0x00,0x00,0x00,
        0x80,0x40,0x20,0x10,0x08,0x06,0x01,0x00,0x01,0x46,0x80,0x40,0x3F,0x00,0x00,0x00//为
    
    // 0x00,0x00,0x7F,0xFC,0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,0x11,0x00,0x11,0xF8,
    // 0x11,0x00,0x11,0x00,0x11,0x00,0x11,0x00,0x11,0x00,0x11,0x00,0xFF,0xFE,0x00,0x00,//正
//     0x02,0x00,0x01,0x00,0x3F,0xFC,0x20,0x04,0x42,0x08,0x02,0x00,0x02,0x00,0xFF,0xFE,
// 0x04,0x20,0x08,0x20,0x18,0x40,0x06,0x40,0x01,0x80,0x02,0x60,0x0C,0x10,0x70,0x08,//安

};

unsigned char str_ascii[16]=
{
    0x00,0x00,0x00,0x10,0x10,0x18,0x28,0x28,0x24,0x3C,0x44,0x42,0x42,0xE7,0x00,0x00,


};



unsigned char key[8] = 
{
    0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01
};

/*
    16*16的横向字库 转化为纵向字库
    wide_str：输入的横向字模
    long_str：输出的纵向字模
*/

void WordModel_WideTransTolong_FONT(unsigned char *wide_str , unsigned char *long_str)
{
    unsigned char buff[16][16];

    // /////////////打印横向字模/////////////
    // printf("widetitude: \n");
    // for (int j = 0; j < 32; j++) {
    // 		printf("%#02X, ", wide_str[j]);
    // 	}
	// 	printf("\n");

    /////////读取点阵/////////////
    for (int kx = 0; kx < 16; kx++)//16行
    {
        for (int jx = 0; jx < 2; jx++)//每一行有两个字节
        {
            for (int ix = 0; ix < 8; ix++)//每个字节有8位
            {
                buff[kx][ix+(jx*8)] = (wide_str[kx * 2 + jx] & key[ix]) ? 1:0;

                // printf("%s", buff[kx][ix +(jx*8)]? "#" : " " );

             }
        }
        //printf("\n");
    }
    ///////////转化为纵向字模/////////////////////

    #ifndef BITS_NUM
    #define BITS_NUM 8 //一个字节8位
    #endif
    #define ROW_BYTE_NUM_16X16 16 //每行的字节数 
    

    for(int row = 0; row <2; row++)//纵向分两行
    {
         for(int bytnum = 0 ;bytnum < ROW_BYTE_NUM_16X16; bytnum++)//第一行往纵向素组里填入 16个字节
        {
            for(int bits = row*BITS_NUM ; bits < BITS_NUM*(row+1); bits++)
            {
                long_str[bytnum+row*ROW_BYTE_NUM_16X16] +=  buff[bits][bytnum]<<((BITS_NUM*(row+1)-1)-bits);
            }            
        }
     }

    ///////////////打印纵向字模/////////////
    // printf("longtitude: \n");
    // for (int j = 0; j < 32; j++) {
    // 		printf("%#X, ", long_str[j]);
    // 	}
	// 	printf("\n");
}


/*
    8*16的横向字库 转化为纵向字库
    wide_str：输入的横向字模
    long_str：输出的纵向字模

*/
void WordModel_WideTransTolong_ASCII(unsigned char *wide_str, unsigned char *long_str)
{
    
    unsigned char buff[16][8] = {0};

    //////打印横向字模//////
    printf("widetitude: \n");
    for (int j = 0; j < 16; j++) {
    		printf("%#X, ", wide_str[j]);
    	}
		printf("\n");

    ///////读取点阵/////////
    for (int kx = 0; kx < 16; kx++)//16行
    {
            for (int ix = 0; ix < 8; ix++)//每个字节有8位
            {
                buff[kx][ix] = (wide_str[kx] & key[ix]) ? 1:0;
                printf("%s", buff[kx][ix] ? "#":"," );

             }
        printf("\n");
    }
    ///////转换为纵向字模//////
    #define ROW_BYTE_NUM_8X16 8 //每行的字节数

    #ifndef BITS_NUM
    #define BITS_NUM 8 //一个字节8位
    #endif

    for(int row = 0; row <2; row++)//纵向分两行
    {
         for(int bytnum = 0 ;bytnum < ROW_BYTE_NUM_8X16; bytnum++)//第一行往纵向素组里填入 8个字节
        {
            for(int bufbits = row*BITS_NUM ; bufbits < BITS_NUM*(row+1); bufbits++)
            {
                long_str[bytnum+row*ROW_BYTE_NUM_8X16] +=  buff[bufbits][bytnum]<<((BITS_NUM*(row+1)-1)-bufbits);
            }            
        }
     }
    ///////打印生成的纵向字模//////
    printf("longtitude: \n");
    for (int j = 0; j < 16; j++) {
    		printf("%#X, ", long_str[j]);
    	}
		printf("\n");

}





// void main()
// {
//     unsigned char longtitude_font[32] ={0};
//     unsigned char longtitude_ascii[16] ={0};

//     WordModel_WideTransTolong_FONT(str , longtitude_font);

//     WordModel_WideTransTolong_ASCII(str_ascii , longtitude_ascii);

// }



