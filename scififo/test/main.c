#include "scififo.h"

DECLARE_SCIFIFO(sci1, 16);

void test1(void)
{
	uint8_t rxbuffer[] = { 0x01, 0x04, 0xA0, 0x00, 0x00, 0x46, 0x53, 0xF8 };
	for (int i = 0; i < sizeof(rxbuffer); i++) {
		scififo_append(&sci1, rxbuffer[i]);
	}
	for (int i = 0; i < sizeof(rxbuffer); i++) {
		printf("at %d : %02X\n", i, scififo_at(&sci1, i));
	}
}

int main()
{
    int n;
    INIT_SCIFIFO(sci1);
    test1();
#if 0
    uint8_t c = 'A';
    for (int i = 0; i < 10; i++) {
        scififo_append(&sci1, c + i);
    }
    printf("sci1's length: %d\n", scififo_length(&sci1));
    
    uint8_t buf[15];
    memset(buf, 0, sizeof(buf));
    n  = scififo_copy(&sci1, buf, sizeof(buf));
    for (int ix = 0; ix < n; ix++) {
        printf("%02X ", buf[ix]);
    }
    printf("\n");

    for (int ix = 0; ix < n; ix++) {
        printf("at[%02d]:%02X\n", ix, scififo_at(&sci1, ix));
    }

    n = scififo_remove(&sci1, sizeof(buf));
    printf("remove:%d\n", n);
#endif
    return 0;
}
