#include <stdio.h>
#include <stdlib.h>
#include "hzk16_test.h"



static void PrintFont_ArryForamt(unsigned char *c);
static void PrintFont_BinaryForamt(unsigned char *c,  unsigned char *longtitude);
static void function_print(void);
void Functionfile_generate(void);
void BinaryFile_generate(void);




char *flist[] = {
"HZK16",
"hzk16f",
"hzk16h",
"hzk16k",
"hzk16l",
"hzk16s",
"hzk16y",
};


/*读取文件 编码*/
void get_hzk_code(char *fname, unsigned char *c, unsigned char buff[])
{
    FILE *HZK;
    if((HZK = fopen(fname, "rb")) == NULL) {
        printf("Can't open font file!");
    }

    int hv = *c - 0xa1;
    int lv = *(c+1) - 0xa1;
    long offset = (long) (94 * hv + lv) * 32;

    fseek(HZK, offset, SEEK_SET);
    fread(buff, 32, 1, HZK);
    fclose(HZK);
}

/*保存文件 编码*/
void save_hzkv_code(char *fname, unsigned char *c, unsigned char SaveBuff[])
{
    FILE *HZKb;
    if((HZKb = fopen(fname, "wb")) == NULL) {
        printf("Can't open font file!");
    }
    int hv = *c - 0xa1;
    int lv = *(c+1) - 0xa1;
    long offset = (long) (94 * hv + lv) * 32;
    fseek(HZKb, offset, SEEK_SET);
    fwrite(SaveBuff, 32, 1, HZKb);
    fclose(HZKb);
}


int main()
{
    
    //Function_generate();//font.c文件生成

    
    BinaryFile_generate();//二进制文件生成

    

    return 0;

}

/*
    按照c文件格式生成HZK16.c文件
*/
void Functionfile_generate()
{
    unsigned char c_hv = 0XB0;
    unsigned char c_lv = 0XB2;
    unsigned char hv_lv[2]={c_hv, c_lv};
    
    printf("#include \"font.h\"\n\n"); 
    printf("const struct font_16x16_t font_16x16[] = {\n");  
   
    	
    for(c_hv = 0XB0 ;c_hv <= 0XF7 ; c_hv++)
    {
         for(c_lv = 0XA1 ;c_lv <= 0XFE ; c_lv++)
        {
            hv_lv[0] = c_hv;
            hv_lv[1] = c_lv;

            PrintFont_ArryForamt(hv_lv);
        }
    }
    printf("}\n");

    function_print();
}

static void PrintFont_ArryForamt(unsigned char *c)
{
    unsigned char longtitude[32] = {0};
    unsigned char WideStr[32]    = {0};
    unsigned short ccc = ((c[0]<<8)+(c[1]));
    
     get_hzk_code("HZK16", c, WideStr);
     WordModel_WideTransTolong_FONT(WideStr , longtitude);
    
    for(int i=0; i<32; i++)
    {
        if(i == 0)
        {
            printf("    0X%04X { 0X%02x,", ccc ,longtitude[i] );
        }
        else if(i == 16)
        {
                    printf("\n             0X%02x,",longtitude[i]);
        }
        else if (i == 31)
        {
            printf("0X%02x}\n",longtitude[i]);
        }     
        else
        {
            printf("0X%02x,",longtitude[i]);
        }
    }
}

static void function_print()
{
    printf("const int font_8x16_count = sizeof(font_8x16) / sizeof(font_8x16[0]);\n");
    printf("const int font_16x16_count = sizeof(font_16x16) / sizeof(font_16x16[0]); \n\n");
    
    printf("const unsigned char * ascii_index(unsigned char c)\n"); 
    printf("{\n");
    printf("    int i;\n");
    printf("    for (i = 0; i < font_8x16_count; i++) {\n");
    printf("        if (font_8x16[i].font_n == c) {\n");
    printf("            return font_8x16[i].font_c;\n");
    printf("        }\n");
    printf("    }\n");
    printf("    return font_8x16[0].font_c;\n");
    printf("}\n");
    printf("\n");


    printf("const unsigned char * font_index(unsigned short font_n)\n"); 
    printf("{\n");
    printf("    int i;\n");
    printf("    if(font_n < 0XB0A1 && font_n > 0XF7FE) \n");
    printf("    {\n");
    printf("         return font_16x16[0].font_c;\n");
    printf("    }\n");
    printf("    for (i = 0; i < font_16x16_count; i++) {\n");
    printf("        if (font_16x16[i].font_n == font_n) {\n");
    printf("            return font_16x16[i].font_c;\n");
    printf("        }\n");
    printf("    }\n");
    printf("    return font_16x16[0].font_c;\n");  
    printf("}\n");   
         
}


/*
二进制文件 hzk16_vb 生成
*/
void BinaryFile_generate()
{
    unsigned char c_hv = 0XA1;
    unsigned char c_lv = 0XA1;
    unsigned char hv_lv[2]={c_hv, c_lv};
    unsigned char longarry[32]={0};

    //unsigned char tamp[32]={0};//test
    
     for(c_hv = 0XA1 ;c_hv <= 0XFE ; c_hv++)
    {
          for(c_lv = 0XA1 ;c_lv <= 0XFE ; c_lv++)
         {
             hv_lv[0] = c_hv;
             hv_lv[1] = c_lv;

            PrintFont_BinaryForamt(hv_lv, longarry);
            save_hzkv_code("hzk16_vb", hv_lv, longarry);            
        }
    }
   
}

static void PrintFont_BinaryForamt(unsigned char *c,  unsigned char *longtitude)
{
    unsigned char WideStr[32]    = {0};
    unsigned short ccc = ((c[0]<<8)+(c[1]));

     get_hzk_code("HZK16", c, WideStr);
     WordModel_WideTransTolong_FONT(WideStr , longtitude);
    
    // for(int i=0; i<32; i++)
    // {
        
    //     printf("%#x, ",longtitude[i]);
        
    // }

    // printf("\n");
}

