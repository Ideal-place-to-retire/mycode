#include "stdio.h"
#include "Memory_managment.h"





#define  OS_CFG_DBG_EN                   0u      //调试使能
#define  OS_SAFETY_CRITICAL              1u     //判断传入 错误指针 是否为0
#define OS_CFG_ARG_CHK_EN                1u      //使能传入参数检查


//////////////以下内容直接 照搬UCOS3的 相关变量 保证编译能通过，如有统一需求，就把下面的内容对应更改////////////////////////////////////////////////////////////////////

#define  CPU_TYPE_CREATE(char_1, char_2, char_3, char_4)        (((CPU_INT32U)((CPU_INT08U)(char_1)) << (0u * 8)) | \
                                                                 ((CPU_INT32U)((CPU_INT08U)(char_2)) << (1u * 8)) | \
                                                                 ((CPU_INT32U)((CPU_INT08U)(char_3)) << (2u * 8)) | \
                                                                 ((CPU_INT32U)((CPU_INT08U)(char_4)) << (3u * 8)))
#define  OS_OBJ_TYPE_MEM                 (OS_OBJ_TYPE)CPU_TYPE_CREATE('M', 'E', 'M', ' ')


#define OS_CRITICAL_ENTER()                     //进入临界区
#define OS_CRITICAL_EXIT_NO_SCHED()             //退出临界区
#define CPU_CRITICAL_EXIT() 
#define CPU_CRITICAL_ENTER()




#ifndef CPU_INT08U
typedef  unsigned  char                 CPU_INT08U;           /*  8-bit unsigned integer  */

#endif

#ifndef CPU_INT32U
typedef  unsigned  int                  CPU_INT32U;           /* 32-bit unsigned integer  */
typedef  CPU_INT32U                     CPU_DATA;
typedef  CPU_INT32U                     CPU_ADDR;

#endif
/////////////////////////////////////---END---////////////////////////////////////////////////////////



#if OS_CFG_DBG_EN > 0u
        OS_MEM                           *OSMemDbgListPtr;                          //调试列表指针
#endif

#ifndef OS_OBJ_QTY
typedef   uint16_t                      OS_OBJ_QTY;           /* Number of kernel objects counter <16>/32 */
#endif
OS_OBJ_QTY                              OSMemQty = (OS_OBJ_QTY)0;                  /* 创建的内存块数量 类型U16 Number of memory partitions created   */


/*$PAGE*/
/*添加内存块 到调试列表，目前不知道有什么用
************************************************************************************************************************
*                                           ADD MEMORY PARTITION TO DEBUG LIST
*
* Description : This function is called by OSMemCreate() to add the memory partition to the debug table.
*
* Arguments   : p_mem    Is a pointer to the memory partition
*
* Returns     : none
*
* Note(s)    : This function is INTERNAL to uC/OS-III and your application should not call it.
************************************************************************************************************************
*/

#if OS_CFG_DBG_EN > 0u
void  OS_MemDbgListAdd (OS_MEM  *p_mem)
{
    p_mem->DbgPrevPtr               = (OS_MEM *)0;
    if (OSMemDbgListPtr == (OS_MEM *)0) {
        p_mem->DbgNextPtr           = (OS_MEM *)0;
    } else {
        p_mem->DbgNextPtr           =  OSMemDbgListPtr;
        OSMemDbgListPtr->DbgPrevPtr =  p_mem;
    }
    OSMemDbgListPtr                 =  p_mem;
}
#endif





/*
************************************************************************************************************************
*                                               CREATE A MEMORY PARTITION
*
* Description : Create a fixed-sized memory partition that will be managed by uC/OS-III.
*
* Arguments   : p_mem    is a pointer to a memory partition control block which is allocated in user memory space.
*
*               p_name   is a pointer to an ASCII string to provide a name to the memory partition.
*
*               p_addr   is the starting address of the memory partition
*
*               n_blks   is the number of memory blocks to create from the partition.
*
*               blk_size is the size (in bytes) of each block in the memory partition.
*
*               p_err    is a pointer to a variable containing an error message which will be set by this function to
*                        either:
*
*                            OS_ERR_NONE                    if the memory partition has been created correctly.
*                            OS_ERR_ILLEGAL_CREATE_RUN_TIME if you are trying to create the memory partition after you
*                                                             called OSSafetyCriticalStart().
*                            OS_ERR_MEM_INVALID_BLKS        user specified an invalid number of blocks (must be >= 2)
*                            OS_ERR_MEM_INVALID_P_ADDR      if you are specifying an invalid address for the memory
*                                                           storage of the partition or, the block does not align on a
*                                                           pointer boundary
*                            OS_ERR_MEM_INVALID_SIZE        user specified an invalid block size
*                                                             - must be greater than the size of a pointer
*                                                             - must be able to hold an integral number of pointers
* Returns    : none
************************************************************************************************************************
*/

void  OSMemCreate (OS_MEM       *p_mem,
                   CPU_CHAR     *p_name,
                   void         *p_addr,
                   OS_MEM_QTY    n_blks,
                   OS_MEM_SIZE   blk_size,
                   OS_MEM_ERR       *p_err)
{
#if OS_CFG_ARG_CHK_EN > 0u
    CPU_DATA       align_msk;
#endif
    OS_MEM_QTY     i;
    OS_MEM_QTY     loops;
    CPU_INT08U    *p_blk;
    void         **p_link;
    



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_MEM_ERR *)0) {
       // assert("err point is 0 \n");
        return;
    }
#endif


#if OS_CFG_ARG_CHK_EN > 0u
    if (p_addr == (void *)0) {                              /* Must pass a valid address for the memory part.         */
       *p_err   = OS_ERR_MEM_INVALID_P_ADDR;
        return;
    }
    if (n_blks < (OS_MEM_QTY)2) {                           /* Must have at least 2 blocks per partition              */
       *p_err = OS_ERR_MEM_INVALID_BLKS;
        return;
    }
    if (blk_size < sizeof(void *)) {                        /* Must contain space for at least a pointer              */
       *p_err = OS_ERR_MEM_INVALID_SIZE;
        return;
    }
    align_msk = sizeof(void *) - 1u;
    if (align_msk > 0) {
        if (((CPU_ADDR)p_addr & align_msk) != 0u){          /* Must be pointer size aligned                           */
           *p_err = OS_ERR_MEM_INVALID_P_ADDR;
            return;
        }
        if ((blk_size & align_msk) != 0u) {                 /* Block size must be a multiple address size             */
           *p_err = OS_ERR_MEM_INVALID_SIZE;
            return;
        }
    }
#endif

    p_link = (void **)p_addr;                               /* Create linked list of free memory blocks               */
    p_blk  = (CPU_INT08U *)p_addr;
    loops  = n_blks - 1u;
    for (i = 0u; i < loops; i++) {
        p_blk +=  blk_size;
       *p_link = (void  *)p_blk;                            /* Save pointer to NEXT block in CURRENT block            */
        p_link = (void **)(void *)p_blk;                    /* Position     to NEXT block                             */
    }
   *p_link             = (void *)0;                         /* Last memory block points to NULL                       */

    OS_CRITICAL_ENTER();
    p_mem->Type        = OS_OBJ_TYPE_MEM;                   /* Set the type of object                                 */
    p_mem->NamePtr     = p_name;                            /* Save name of memory partition                          */
    p_mem->AddrPtr     = p_addr;                            /* Store start address of memory partition                */
    p_mem->FreeListPtr = p_addr;                            /* Initialize pointer to pool of free blocks              */
    p_mem->NbrFree     = n_blks;                            /* Store number of free blocks in MCB                     */
    p_mem->NbrMax      = n_blks;
    p_mem->BlkSize     = blk_size;                          /* Store block size of each memory blocks                 */

#if OS_CFG_DBG_EN > 0u
    OS_MemDbgListAdd(p_mem);
#endif

    OSMemQty++;

    OS_CRITICAL_EXIT_NO_SCHED();
   *p_err = OS_ERR_NONE;
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                                  GET A MEMORY BLOCK
*
* Description : Get a memory block from a partition
*
* Arguments   : p_mem   is a pointer to the memory partition control block
*
*               p_err   is a pointer to a variable containing an error message which will be set by this function to
*                       either:
*
*                       OS_ERR_NONE               if the memory partition has been created correctly.
*                       OS_ERR_MEM_INVALID_P_MEM  if you passed a NULL pointer for 'p_mem'
*                       OS_ERR_MEM_NO_FREE_BLKS   if there are no more free memory blocks to allocate to the caller
*
* Returns     : A pointer to a memory block if no error is detected
*               A pointer to NULL if an error is detected
************************************************************************************************************************
*/

void  *OSMemGet (OS_MEM  *p_mem,
                 OS_MEM_ERR  *p_err)
{
    void    *p_blk;

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_MEM_ERR *)0) {
        // printf("err point is 0 \n");
        return ((void *)0);
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mem == (OS_MEM *)0) {                             /* Must point to a valid memory partition                 */
       *p_err  = OS_ERR_MEM_INVALID_P_MEM;
        return ((void *)0);
    }
#endif

    CPU_CRITICAL_ENTER();
    if (p_mem->NbrFree == (OS_MEM_QTY)0) {                  /* See if there are any free memory blocks                */
        CPU_CRITICAL_EXIT();
       *p_err = OS_ERR_MEM_NO_FREE_BLKS;                    /* No,  Notify caller of empty memory partition           */
        return ((void *)0);                                 /*      Return NULL pointer to caller                     */
    }
    p_blk              = p_mem->FreeListPtr;                /* Yes, point to next free memory block                   */
    p_mem->FreeListPtr = *(void **)p_blk;                   /*      Adjust pointer to new free list                   */
    p_mem->NbrFree--;                                       /*      One less memory block in this partition           */
    CPU_CRITICAL_EXIT();
   *p_err = OS_ERR_NONE;                                    /*      No error                                          */
    return (p_blk);                                         /*      Return memory block to caller                     */
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                                 RELEASE A MEMORY BLOCK
*
* Description : Returns a memory block to a partition
*
* Arguments   : p_mem    is a pointer to the memory partition control block
*
*               p_blk    is a pointer to the memory block being released.
*
*               p_err    is a pointer to a variable that will contain an error code returned by this function.
*
*                            OS_ERR_NONE               if the memory block was inserted into the partition
*                            OS_ERR_MEM_FULL           if you are returning a memory block to an already FULL memory
*                                                      partition (You freed more blocks than you allocated!)
*                            OS_ERR_MEM_INVALID_P_BLK  if you passed a NULL pointer for the block to release.
*                            OS_ERR_MEM_INVALID_P_MEM  if you passed a NULL pointer for 'p_mem'
************************************************************************************************************************
*/

void  OSMemPut (OS_MEM  *p_mem,
                void    *p_blk,
                OS_MEM_ERR  *p_err)
{

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_MEM_ERR *)0) {
         // printf("err point is 0 \n");
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mem == (OS_MEM *)0) {                             /* Must point to a valid memory partition                 */
       *p_err  = OS_ERR_MEM_INVALID_P_MEM;
        return;
    }
    if (p_blk == (void *)0) {                               /* Must release a valid block                             */
       *p_err  = OS_ERR_MEM_INVALID_P_BLK;
        return;
    }
#endif

    CPU_CRITICAL_ENTER();
    if (p_mem->NbrFree >= p_mem->NbrMax) {                  /* Make sure all blocks not already returned              */
        CPU_CRITICAL_EXIT();
       *p_err = OS_ERR_MEM_FULL;
        return;
    }
    *(void **)p_blk    = p_mem->FreeListPtr;                /* Insert released block into free block list             */
    p_mem->FreeListPtr = p_blk;
    p_mem->NbrFree++;                                       /* One more memory block in this partition                */
    CPU_CRITICAL_EXIT();
   *p_err              = OS_ERR_NONE;                       /* Notify caller that memory block was released           */
}






//定义一个存储区
OS_MEM INTERNAL_MEM;	
//存储区中存储块数量
#define INTERNAL_MEM_NUM		5
//每个存储块大小
//由于一个指针变量占用4字节所以块的大小一定要为4的倍数
//而且必须大于一个指针变量(4字节)占用的空间,否则的话存储块创建不成功
#define INTERNAL_MEMBLOCK_SIZE	100  
//存储区的内存池，使用内部RAM
CPU_INT08U Internal_RamMemp[INTERNAL_MEM_NUM][INTERNAL_MEMBLOCK_SIZE];




void main()
{
    OS_MEM_ERR err;
    CPU_INT08U *internal_buf;
    CPU_INT08U *buf1, *buf2, *buf3, *buf4, *buf5;
 
    CPU_INT08U i;

/////创建内存区
    OSMemCreate((OS_MEM*	)&INTERNAL_MEM,
				(CPU_CHAR*	)"Internal Mem",
				(void*		)&Internal_RamMemp[0][0],
				(OS_MEM_QTY	)INTERNAL_MEM_NUM,
				(OS_MEM_SIZE)INTERNAL_MEMBLOCK_SIZE,
				(OS_MEM_ERR*	)&err);

/////申请内存块
    internal_buf = OSMemGet((OS_MEM*)&INTERNAL_MEM, (OS_MEM_ERR*)&err);

    if(err == OS_ERR_NONE) //内存申请成功
    {
        printf("internal_buf add :%#x\r\n",(uint32_t)(internal_buf));
        printf("Memory Get success!  ");
        internal_buf[0]  = 1;
        internal_buf[30] = 3;
        internal_buf[50] = 5;
        internal_buf[80] = 7;
        internal_buf[99] = 9;
        printf("inter buf :%s \n",internal_buf); 
        printf("inter buf :%d \n",internal_buf[0]); 
        printf("inter buf :%d \n",internal_buf[99]); 
    }
    if(err == OS_ERR_MEM_NO_FREE_BLKS) //内存块不足
    {
        printf("Memory Get FAIL!  ");
    }
/////释放内存块
    if(internal_buf != NULL) //internal_buf不为空就释放内存
    {
        OSMemPut((OS_MEM*	)&INTERNAL_MEM,		//释放内存
                    (void*		)internal_buf,
                    (OS_MEM_ERR* 	)&err);
        printf("internal_buf release add :%#x\r\n",(uint32_t)(internal_buf));
        printf("Memory Put success!  \n ");
    }

/********************批量多次申请 *************************************************************/
    #define BUFF(i)     buf##i
    for(i=0; i<2; i++)
    {
        BUFF(1) = OSMemGet((OS_MEM*)&INTERNAL_MEM, (OS_MEM_ERR*)&err);
        BUFF(2) = OSMemGet((OS_MEM*)&INTERNAL_MEM, (OS_MEM_ERR*)&err);
        BUFF(3) = OSMemGet((OS_MEM*)&INTERNAL_MEM, (OS_MEM_ERR*)&err);
        BUFF(4) = OSMemGet((OS_MEM*)&INTERNAL_MEM, (OS_MEM_ERR*)&err);
        BUFF(5) = OSMemGet((OS_MEM*)&INTERNAL_MEM, (OS_MEM_ERR*)&err);

        if(err == OS_ERR_NONE) //内存申请成功
        {
            printf("BUFF(1) add :%#x\r\n",(uint32_t)(BUFF(1)));
            printf("BUFF(2) add :%#x\r\n",(uint32_t)(BUFF(2)));
            printf("BUFF(3) add :%#x\r\n",(uint32_t)(BUFF(3)));
            printf("BUFF(4) add :%#x\r\n",(uint32_t)(BUFF(4)));
            printf("BUFF(5) add :%#x\r\n",(uint32_t)(BUFF(5)));
            printf("Memory Get success!  \n");
            BUFF(5)[0]  = 11;
           
            BUFF(5)[99] = 99;
            
            printf("BUFF(5) buf :%d \n",BUFF(5)[0]); 
            printf("BUFF(5) buf :%d \n",BUFF(5)[99]); 
        }
        if(err == OS_ERR_MEM_NO_FREE_BLKS) //内存块不足
        {
            printf("Memory Get FAIL! MAX num: %d \n",INTERNAL_MEM.NbrMax);
            printf("Memory Get FAIL! NbrFree: %d \n",INTERNAL_MEM.NbrFree);
        }

        if(BUFF(5) != NULL) //internal_buf不为空就释放内存
        {
            OSMemPut((OS_MEM*	)&INTERNAL_MEM,		//释放内存
                        (void*		)BUFF(5),
                        (OS_MEM_ERR* 	)&err);
            printf("BUFF(5) release add :%#x\r\n",(uint32_t)(BUFF(5)));
            printf("Memory Put success!   \n");
        }

        }


}









