#ifndef  _RS485PARSE_H
#define  _RS485PARSE_H

#include "stdtype.h"
#include "stdint.h"

typedef struct {
	void (*init)(void);
	void (*clear)(void);
    void (*append)(uint8_t c);
    void (*remove)(uint16_t n);
    uint8_t (*at)(uint16_t n);
    void (*copy)(void *des, uint16_t len);
    uint16_t (*length)(void);
    uint8_t *(*getptr)(void);
    void (*set)(uint16_t n, uint8_t c);
} STR_RS485_SCI_BUFFER;


extern const STR_RS485_SCI_BUFFER * pRS485SciBuf;


UINT8  RS485MakeFrame(uint8_t *buffer);


#endif
