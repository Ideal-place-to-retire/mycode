/*! 
*  \file ScreenHandle.h
*  
*/

#ifndef _RS485HANDLE_H
#define _RS485HANDLE_H


#include "stdint.h"

#define BMS_MSG_ASK_PERIOD_MS	800

#define RS485_CMD_BUFF_SIZE	  20

#define	RS485_MESSAGE

#define RS485_HEAD1 0X43		// 报文帧头1
#define RS485_HEAD2 0X57  		// 报文帧头2

#define RS485_TAIL1 0X45		// 报文帧尾1
#define RS485_TAIL2 0X53      	// 报文帧尾2


#define BSM_NUM                     2         //BMS数量


#define MAIN_BMS_FUNCTION           0x01      //主功能码
#define MAIN_ADDR_FUNCTION          0x10      //主功能吗

#define SECONDARY_FUNCTION          0x00      //子功能码

#define BMS_DATA_LENGTH             0x00      //数据长度
#define ADDR_DATA_LENGTH            0x01      //数据长度




typedef struct 
{
	uint16_t SOC;						//SOC
	uint16_t TotalVoltage;			    //总电压
	uint16_t HighestCellVoltage;		//最高单体电压值
	uint16_t LowestCellVoltage;		    //最低单体电压值
	uint16_t Voltage;					//压差
	uint16_t Temperature;				//温度
	uint16_t ChargerStatus;			    //充电器状态
	uint8_t  Fault[9];					//故障
	uint8_t  MajorVersion;				//主版本号
	uint8_t  MinorVersion;				//次版本号
    uint8_t  OfflinCount;                //离线计数
    uint8_t  OfflinStatus ;              //离线状态
	uint8_t  ChargeFaultStatus ;              //充电器故障状态标志位	
	uint8_t  BmsFaultStatus ;                 //BMS状态
	uint8_t  BmsFaultCount ;                 //BMS故障状态计数
	uint8_t  BMSBatLowFlag;					//	BMS电量低标志位
}BmsMsg;		


void RS485CmdHandle(uint8_t *buf);
uint16_t GetSOC(uint8_t SlaveAddr);
void BmsAskMsgSend (uint8_t SlaveAddr);
void BmsOfflineCheck(uint8_t add);


#endif
