
#include "RS485Parse.h"
#include "string.h"
#include "c_mem.h"
#include "saecrc.h"
#include "RS485Handle.h"
#include "SEGGER_RTT.h"
#include "systick.h"


#define RS485_BUFFER_SIZE 2048
#define RS485_BUFFER_BOUND (RS485_BUFFER_SIZE-1)
static uint8_t gRS485Buffer[RS485_BUFFER_SIZE];
static uint16_t pr, pw;


static void _RS485BufferInit(void)
{
	pr = pw = 0;
	c_bzero(gRS485Buffer, sizeof(gRS485Buffer));
}

static void _RS485BufferClear()
{
	pr = pw = 0;
	c_bzero(gRS485Buffer, sizeof(gRS485Buffer));
}



static void _RS485BufferAppend(uint8_t c)
{
	gRS485Buffer[pw] = c;
	pw = (pw + 1)&RS485_BUFFER_BOUND;
}

static uint16_t _RS485BufferLength(void)
{
	return (pw - pr) & RS485_BUFFER_BOUND;
}

static void _RS485BufferCopy(void *des, uint16_t len)
{
	uint16_t i;
	uint8_t *p = (uint8_t *)des;
	for (i = 0; i < len; i++) {
		*p++ = gRS485Buffer[(pr + i) & RS485_BUFFER_BOUND];
	}
}

static void _RS485BufferRemove(uint16_t n)
{
	uint16_t usLen;
	usLen = _RS485BufferLength();
	if (n < usLen) {
		usLen = n;
	}
	pr = (pr + usLen) & RS485_BUFFER_BOUND;
}

static uint8_t _RS485BufferAt(uint16_t n)
{
	return gRS485Buffer[(pr + n) & RS485_BUFFER_BOUND];
}



static uint8_t * _RS485BufferGetPtr(void)
{
    return gRS485Buffer;
}

 

void _RS485BufferSet(uint16_t n, uint8_t c)
{
	gRS485Buffer[(pw + n)&RS485_BUFFER_BOUND] = c;
}

/* define crc8 in other place */
uint32_t sae_crc8_cal (void *ptr, uint16_t usLength)
{
	uint8_t i, j;
	uint8_t ucCrc;
	uint8_t ucPoly;

	uint8_t *p = (uint8_t *)ptr;
	ucCrc = 0xFF;
	ucPoly = 0x1D;

	for (i = 0; i < usLength; i++)
	{
		ucCrc ^= p[i];

		for (j = 0; j < 8; j++)
		{
			if (ucCrc & 0x80)
			{
				ucCrc = (ucCrc << 1) ^ ucPoly;
			}
			else
			{
				ucCrc <<= 1;
			}
		}
	}
	ucCrc ^= (uint8_t)0xFF;
	return ucCrc;
}




const STR_RS485_SCI_BUFFER stRS485Buffer = {
	_RS485BufferInit,
	_RS485BufferClear,
	_RS485BufferAppend,
	_RS485BufferRemove,
	_RS485BufferAt,
	_RS485BufferCopy,
	_RS485BufferLength,
	_RS485BufferGetPtr,
};

const STR_RS485_SCI_BUFFER * pRS485SciBuf = &stRS485Buffer;


UINT8 TestCmd[] = {0X43 ,0X57 ,0X01 ,0X01 ,0X00, 0X19, 00 ,00, 00, 00 ,00 ,00, 00, 00 ,00 ,00 ,00, 00, 00, 02 ,00, 00 ,00 ,00 ,00 ,00 ,00, 0x0B, 01 ,01 ,00 ,0X64, 0X45,0X53 };


#define HEAD1	0
#define HEAD2	1
#define DATA_LENGTH	5
#define CRC		(5 + pRS485SciBuf->at(5) + 1)
#define TAIL1	(4 + 1 + pRS485SciBuf->at(5) + 2)
#define TAIL2	(4 + 1 + pRS485SciBuf->at(5) + 3)

#define FRAM_LONG  (9 + pRS485SciBuf->at(5))			// 帧的总长
#define DATA_LONG  (3 + 1 + pRS485SciBuf->at(5))		// 从机地址（3）+长度（1）+数据（pRS485SciBuf->at(5)）数据长度
#define CRC_LONG   (2 + DATA_LONG)						// 帧头 + 从机地址开始到CRC前的长度

UINT8  RS485MakeFrame(uint8_t *buffer)
{
	UINT8 CrcBuff[20] = {0};
    while(pRS485SciBuf->length() != 0)   
    {		
        if(pRS485SciBuf->at(HEAD1) == RS485_HEAD1)  
        {	
			vdelay_ms(40);			

//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, "--------------\n");
			 
            if(pRS485SciBuf->at(HEAD2) == RS485_HEAD2 &&\
				pRS485SciBuf->at(TAIL1) == RS485_TAIL1 &&\
			    pRS485SciBuf->at(TAIL2) == RS485_TAIL2) 
            {
				for(int i = 0; i < DATA_LONG; i++)
				{
                	buffer[i] = pRS485SciBuf->at(i + 2);
//					printf("==%X\n",buffer[i]);
	//DEBUG_LOG_INFO(DEBUG_LEVEL_2, "==%X\n",buffer[i]);

				}

				CrcBuff[0] = RS485_HEAD1;
				CrcBuff[1] = RS485_HEAD2; 
				c_memcpy(&CrcBuff[2], buffer, DATA_LONG);
				
                int crcset = sae_crc8_cal(CrcBuff,CRC_LONG);	//获取crc
				
//				printf("CRC	 %X，，crcset %x  CRC_LONG %d\n",pRS485SciBuf->at(CRC),crcset, CRC_LONG);	
			
                if(pRS485SciBuf->at(CRC) == crcset)
				{
                    printf("CRCOK\n");
					pRS485SciBuf->remove(FRAM_LONG);
					return 1;
                }
                else
				{
					pRS485SciBuf->remove(1);
               		return 0;				
				}
            }
            else
            {
               pRS485SciBuf->remove(1);
            }
        }
        else
        {
            pRS485SciBuf->remove(1);
        }

    }
	return 0;
}


   