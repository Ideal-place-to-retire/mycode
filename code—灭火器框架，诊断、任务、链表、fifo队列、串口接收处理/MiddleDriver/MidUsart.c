#include "MidUsart.h"


/**
 * @brief 串口0 中断回调函数
 *
 * @note 
 */
void Usart0_CallBack(uint8_t *data, uint8_t size)
{
	//DrvUsartBlockSendData(USART_1,TEST_usart1,5 );
}

/**
 * @brief 串口1 中断回调函数
 *
 * @note 
 */
void Usart1_CallBack(uint8_t *data, uint8_t size)
{
	//DrvUsartBlockSendData(USART_1,TEST_usart1,5 );
}





/**
 * @brief usart1初始化
 *
 */
void Usart0_init()
{	
	DRV_USART_CONFIG  UsartConf;
	USART_INT_MANAGE  UsartIntConf;
	
	DrvUsartGpioConfig(USART_0);//GPIO初始化
	
	UsartConf.mBaudRate = 115200;
	UsartConf.mDataBit = USART_WL_8BIT;
	UsartConf.mParityMode = USART_PM_NONE;
	UsartConf.mStopBit = USART_STB_1BIT;
	DrvUsartInit(USART_0,&UsartConf);//USART init
	
	UsartIntConf.Irq = USART_INT_RBNE;
	UsartIntConf.IrqFlag = USART_INT_FLAG_PERR;//not need
	UsartIntConf.IrqType = USART0_IRQn;
	UsartIntConf.nvic_irq_pre = 3;
	UsartIntConf.nvic_irq_sub = 2;
	DrvUsartInterruptConfig(USART_0, &UsartIntConf, ENABLE);//中断配置
	
	DrvUsartInterruptCallBack(USART0_RBNE, Usart0_CallBack);
}





/**
 * @brief usart1初始化
 *
 */
void Usart1_init()
{	
	DRV_USART_CONFIG  UsartConf;
	USART_INT_MANAGE  UsartIntConf;
	
	DrvUsartGpioConfig(USART_1);//GPIO初始化
	
	UsartConf.mBaudRate = 115200;
	UsartConf.mDataBit = USART_WL_8BIT;
	UsartConf.mParityMode = USART_PM_NONE;
	UsartConf.mStopBit = USART_STB_1BIT;
	DrvUsartInit(USART_1,&UsartConf);//USART init
	
	UsartIntConf.Irq = USART_INT_RBNE;
	UsartIntConf.IrqFlag = USART_INT_FLAG_PERR;//not need
	UsartIntConf.IrqType = USART1_IRQn;
	UsartIntConf.nvic_irq_pre = 3;
	UsartIntConf.nvic_irq_sub = 2;
	DrvUsartInterruptConfig(USART_1, &UsartIntConf, ENABLE);//中断配置
	
	DrvUsartInterruptCallBack(USART1_RBNE, Usart1_CallBack);
}
