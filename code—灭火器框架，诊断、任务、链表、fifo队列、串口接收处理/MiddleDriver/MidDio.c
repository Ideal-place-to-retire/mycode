#include "MidDio.h"


#define RCU_LIST_SIZE    sizeof(RCU_INIT_List)/sizeof(RCU_INIT_List[0]) ///< 时钟使能数量
#define IO_LIST_SIZE 	 sizeof(stGpio_list)/sizeof(stGpio_list[0]) ///< IO数量


/**
 * @brief GPIO初始化
 *
 * @param 
 * @note  引脚的时钟使能、重映射、引脚初始化
 */
void DioInit()
{
	uint8_t i ;
	
//初始化引脚时钟
	rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_GPIOC);
    rcu_periph_clock_enable(RCU_GPIOD);
    rcu_periph_clock_enable(RCU_GPIOE);
    rcu_periph_clock_enable(RCU_AF);

//禁用JTAG和NJTRST 使用PB4 PA15
	DrvSWDInit();
	
//初始化引脚
    for(i=0; i < gucGpioInitNbr; i++)
    {
        DrvGpioInit((gd32f307ptr + gstGpioList[i].index)->uwPort, \
        (gd32f307ptr + gstGpioList[i].index)->uwPin, gstGpioList[i].usMode);
    }
	
}

/**
 * @brief GPIO引脚输出高低电平
 *
 * @param index ：操作的gpio引脚索引，详细引脚见 GpioMap.c文件
 * @param Set  ：输出电平
 */
void DioWriteChannel(ENU_GPIO_INDEX index,  uint8_t Set)
{
	DrvGpioOperat(GD32F307GpioList[index].uwPort, GD32F307GpioList[index].uwPin, Set);
}

/**
 * @brief 获取GPIO引脚输入电平
 *
 * @param index ：操作的gpio引脚索引，详细引脚见 GpioMap.c文件
 */

uint8_t DioReadChannel (ENU_GPIO_INDEX index)
{
	return DrvGpioGetInputBit(GD32F307GpioList[index].uwPort, GD32F307GpioList[index].uwPin);
}
