/**
 * @file  diagnostic.h
 * @brief 诊断处理
 * @author liusen
 * @date  2023-6-14
 * @version 0.1
 * @copyright 创为新能源科技股份有限公司
 */
#ifndef _DIAGNOSTIC_H_
#define _DIAGNOSTIC_H_

#include "swp_incl.h"
#include "list.h"

#define DIAGNOSIC_PERIOD_MS  10U  //诊断周期,模块根据需要定义

#define T_DETCT_MIN   (-32767-1)    //INT16_MIN
#define T_DETCT_MAX   (32767)       //INT16_MAX
#define T_DETCT_COUNT INT16
/**
 * @brief 诊断结果
*/
typedef enum DETCT_RESULT
{
    DETCT_FAILED,    //失败
    DETCT_PREFAILED, //预失败
    DETCT_NOTCHECK,  //未检测
    DETCT_PREPASSED, //预通过
    DETCT_PASSED     //通过
} ENU_DETCT_RESULT;  //检测结果

/**
 * @brief 诊断状态字节
*/
typedef union{
    struct{
        UINT8 TestFailed:1;
        UINT8 R1:1;
        UINT8 R2:1;
        UINT8 ConfirmedDTC:1;
        UINT8 R4:1;
        UINT8 R5:1;
        UINT8 testNotCompletedThisOperationCycle :1 ; //测试没有完成这个操作周期
        UINT8 R7:1;
    } DTCStatusBit;
    UINT8 ucDTCStatusByte;
} STR_DTC_STATUS;  

/**
 * @brief 诊断配置数据，含链表结构
*/
typedef struct
{
    STR_DTC_STATUS stDTCStatus;          //检测状态
    UINT16 usPeriod;                   //周期
    UINT16 usPassLimit;                //通过次数
    UINT16 usFailedLimit;              //失败次数
    INT16  sCounter;                  //故障计数器
    UINT32 (*irfDiagFunction)(void);    //故障诊断函数
    void (*irfFaultConfirm)(bool);       //故障确认函数

    struct list_head list;            
} STR_DIAG_CFG_NODE;

INT32 DiagInit(void);                                 //初始化
INT32 DiagReset(void);                                //复位
INT32 DiagAddNode(STR_DIAG_CFG_NODE *diagCfgNodePtr); //增加节点
INT32 DiagDltNode(STR_DIAG_CFG_NODE *diagCfgNodePtr);
void DiagMainProcess(void);                             //主进程

#endif // _DIAGNOSTIC_H_
