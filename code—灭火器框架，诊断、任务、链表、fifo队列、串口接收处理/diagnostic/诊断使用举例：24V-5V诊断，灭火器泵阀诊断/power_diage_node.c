/**
 * 电路检测
*/
#include "power_diage_node.h"
#include "event_queue_Handle.h"
#include "stdio.h"
#include "MidAdc.h"

//test
#include "exth_diage_node.h"

#define  OVERVOLTAGE_24   	26000
#define  UNDERVOLTAGE_24  	22000
#define  OVERVOLTAGE_5  	6000
#define  UNDERVOLTAGE_5  	4000


STR_DIAG_CFG_NODE stDiag24VHFaultCfg;

/**
 * 24v诊断过压
 * 1 = 有故障， 0 = 无故障 
 * 24V过压诊断，如果出现超出正常范围的电压，返回1，如果正常返回0
*/
UINT32  powerNode24VHDiag(void)
{
    uint32_t buf = AdcDmaValu(SAMPLE_24V);
	
	//DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node24VH  DIAG \n");
	
    if (buf > OVERVOLTAGE_24)
	{		
        return 1;  // 有故障
    } 
	    
	return 0;
}



/**
 * 24v过压确认
 * confirmed: 确认状态，1=诊断未通过失败  、 0 = 诊断通过 成功
 * 1 = 有故障， 0 = 无故障 
 * 24V过压确认， 诊断通过或失败 需要执行的操作
*/
void powerNode24VHConfirm(bool  confirmed)
{
     if (confirmed == TRUE) 
	{
        // 诊断未通过失败，执行相应操作       	   
//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node24VH  ABNORMALLLLLLLL \n");
        EvtFaultWrite(FAULT_24V_H);			
    } 	
	 else 
	{
        // 诊断通过成功，执行相应操作
			
		 //DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node24VH NORM\n");
    }	
}



/**
 * 24v诊断欠压
 * 1 = 有故障， 0 = 无故障 
 * 24V欠压诊断，如果出现超出正常范围的电压，返回1，如果正常返回0
*/
UINT32  powerNode24VLDiag(void)
{
    uint32_t buf = AdcDmaValu(SAMPLE_24V); 

//	DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node24VL  DIAG \n");

    if (buf < UNDERVOLTAGE_24)
	{
        return 1;  // 有故障
    } 
	return 0;
}


/**
 * 24v欠压确认
 * confirmed: 确认状态，1=诊断未通过失败  、 0 = 诊断通过 成功
 * 1 = 有故障， 0 = 无故障 
 * 24V欠压确认， 诊断通过或失败 需要执行的操作
*/
void  powerNode24VLConfirm(bool  confirmed)
{
	 if (confirmed == TRUE) 
	{
		// 诊断未通过失败，执行相应操作
//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node24VL  ABNORMALLLLLLLL \n");

		EvtFaultWrite(FAULT_24V_L);
	} 
	 else 
	{
		
	}
}



/**
 * 5v诊断过压
 * 1 = 有故障， 0 = 无故障 
 * 5V过压诊断，如果出现超出正常范围的电压，返回1，如果正常返回0
*/
UINT32  powerNode5VHDiag(void)
{
    uint32_t buf = AdcDmaValu(SAMPLE_24V); 
//	DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node5VH  DIAG \n");

    if (buf > OVERVOLTAGE_5)
	{
        return 1;  // 有故障
    } 
	return 0;
}



/**
 * 5v过压确认
 * confirmed: 确认状态，1=诊断未通过失败  、 0 = 诊断通过 成功
 * 1 = 有故障， 0 = 无故障 
 * 5V过压确认， 诊断通过或失败 需要执行的操作
*/
void  powerNode5VHConfirm(bool  confirmed)
{
	 if (confirmed == TRUE) 
	{
		// 诊断未通过失败，执行相应操作
//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node5VL  ABNORMALLLLLLLL \n");

		EvtFaultWrite(FAULT_5V_H);
	} 
	 else 
	{
		
	}
}






/**
 * 5v诊断欠压
 * 1 = 有故障， 0 = 无故障 
 * 5V欠压诊断，如果出现超出正常范围的电压，返回1，如果正常返回0
*/
UINT32 powerNode5VLDiag( void)
{
    uint32_t buf = AdcDmaValu(SAMPLE_24V); 
//	DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node5VL  DIAG \n");

    if (buf < UNDERVOLTAGE_5)
	{
        return 1;  // 有故障
    } 
	return 0;
}



/**
 * 5v欠压确认
 * confirmed: 确认状态，1=诊断未通过失败  、 0 = 诊断通过 成功
 * 1 = 有故障， 0 = 无故障 
 * 5V欠压确认， 诊断通过或失败 需要执行的操作
*/
void  powerNode5VLConfirm(bool  confirmed)
{
     if (confirmed == TRUE) 
	{
        // 诊断未通过失败，执行相应操作
       
//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " Node5VL  ABNORMALLLLLLLL \n");

        EvtFaultWrite(FAULT_5V_L);
    } 
	 else 
	{
        // 诊断通过成功，执行相应操作
        
    }
}







STR_DIAG_CFG_NODE stDiag24VHFaultCfg = {
	.stDTCStatus = 0,
    .usPeriod = POWER_24VH_FAULT_PERIOD,                  //周期
    .usPassLimit = POWER_24VH_FAULT_PASS_LIMIT,               //通过次数
    .usFailedLimit = POWER_24VH_FAULT_FAILED_LIMIT,             //失败次数
    .sCounter = 0,                 //故障计数器
    .irfDiagFunction = powerNode24VHDiag,    //故障诊断函数
    .irfFaultConfirm = powerNode24VHConfirm,       //故障确认函数
	
};

STR_DIAG_CFG_NODE stDiag24VLFaultCfg = {
	.stDTCStatus = 0,
    .usPeriod = POWER_24VL_FAULT_PERIOD,                  //周期
    .usPassLimit = POWER_24VL_FAULT_PASS_LIMIT,               //通过次数
    .usFailedLimit = POWER_24VL_FAULT_FAILED_LIMIT,             //失败次数
    .sCounter = 0,                 //故障计数器
    .irfDiagFunction = powerNode24VLDiag,    //故障诊断函数
    .irfFaultConfirm = powerNode24VLConfirm,       //故障确认函数
	
};

STR_DIAG_CFG_NODE stDiag5VHFaultCfg = {
	.stDTCStatus = 0,
    .usPeriod = POWER_5VH_FAULT_PERIOD,                  //周期
    .usPassLimit = POWER_5VH_FAULT_PASS_LIMIT,               //通过次数
    .usFailedLimit =POWER_5VH_FAULT_FAILED_LIMIT,             //失败次数
    .sCounter = 0,                 //故障计数器
    .irfDiagFunction = powerNode5VHDiag,    //故障诊断函数
    .irfFaultConfirm = powerNode5VHConfirm,       //故障确认函数
	
};

STR_DIAG_CFG_NODE stDiag5VLFaultCfg = {
	.stDTCStatus = 0,
    .usPeriod = POWER_5VL_FAULT_PERIOD,                  //周期
    .usPassLimit = POWER_5VL_FAULT_PASS_LIMIT,               //通过次数
    .usFailedLimit = POWER_5VL_FAULT_FAILED_LIMIT,             //失败次数
    .sCounter = 0,                 //故障计数器
    .irfDiagFunction = powerNode5VLDiag,    //故障诊断函数
    .irfFaultConfirm = powerNode5VLConfirm,       //故障确认函数
	
};




/**
 * 电路诊断初始化
 * 将电路诊断的各个节点，添加到诊断列表里
*/
void diagPowerNodeInit(){

    DiagAddNode(&stDiag24VHFaultCfg);
    DiagAddNode(&stDiag24VLFaultCfg);
    DiagAddNode(&stDiag5VHFaultCfg);
    DiagAddNode(&stDiag5VLFaultCfg);
    
}
