#ifndef _POWER_DIAGE_NODE_H
#define _POWER_DIAGE_NODE_H

#include "diagnostic.h"

//#include "middle_adc.c"
//#include "middle_adc.h"
//#include "event_queue.h"


#include <stdint.h>
	
	
/**/
//24V过压诊断周期
#define  POWER_24VH_FAULT_PERIOD  100
//24V过压诊断通过持续时间                                  
#define  POWER_24VH_FUALT_PASS    2000
//24V过压诊断失败持续时间                                                                     
#define  POWER_24VH_FAULT_FAILED	2000
//24V过压诊断通过次数                                                           
#define  POWER_24VH_FAULT_PASS_LIMIT    (POWER_24VH_FUALT_PASS / POWER_24VH_FAULT_PERIOD)      
//24V过压诊断失败次数  
#define  POWER_24VH_FAULT_FAILED_LIMIT  (POWER_24VH_FAULT_FAILED / POWER_24VH_FAULT_PERIOD)  

//24V欠压诊断周期
#define  POWER_24VL_FAULT_PERIOD 	100
//24V欠压诊断通过持续时间                                  
#define  POWER_24VL_FUALT_PASS   	2000
//24V欠压诊断失败持续时间                                                                     
#define  POWER_24VL_FAULT_FAILED      2000
//24V欠压诊断通过次数                                                           
#define  POWER_24VL_FAULT_PASS_LIMIT (POWER_24VL_FUALT_PASS / POWER_24VL_FAULT_PERIOD)      
//24V欠压诊断失败次数  
#define  POWER_24VL_FAULT_FAILED_LIMIT (POWER_24VL_FAULT_FAILED / POWER_24VL_FAULT_PERIOD)  

//5V过压诊断周期
#define  POWER_5VH_FAULT_PERIOD  	100
//5V过压诊断通过持续时间                                  
#define  POWER_5VH_FUALT_PASS   	2000
//5V过压诊断失败持续时间                                                                     
#define  POWER_5VH_FAULT_FAILED 	2000
//5V过压诊断通过次数                                                           
#define  POWER_5VH_FAULT_PASS_LIMIT (POWER_5VH_FUALT_PASS/POWER_5VH_FAULT_PERIOD)      
//5V过压诊断失败次数  
#define  POWER_5VH_FAULT_FAILED_LIMIT (POWER_5VH_FAULT_FAILED / POWER_5VH_FAULT_PERIOD)  

//5V欠压诊断周期
#define  POWER_5VL_FAULT_PERIOD   	100
//5V欠压诊断通过持续时间                                  
#define  POWER_5VL_FUALT_PASS 		2000
//5V欠压诊断失败持续时间                                                                     
#define  POWER_5VL_FAULT_FAILED 	2000
//5V欠压诊断通过次数                                                           
#define  POWER_5VL_FAULT_PASS_LIMIT  (POWER_5VL_FUALT_PASS/POWER_5VL_FAULT_PERIOD)      
//5V欠压诊断失败次数  
#define  POWER_5VL_FAULT_FAILED_LIMIT  (POWER_5VL_FAULT_FAILED/POWER_5VL_FAULT_PERIOD)  




void diagPowerNodeInit(void);



#endif



