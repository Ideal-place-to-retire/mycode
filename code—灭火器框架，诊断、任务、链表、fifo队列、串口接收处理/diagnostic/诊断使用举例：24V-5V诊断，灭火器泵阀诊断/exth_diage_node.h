#ifndef _EXTH_DIAGE_NODE_H
#define _EXTH_DIAGE_NODE_H


#include "diagnostic.h"
#include "stdint.h"


/**/
//启泵诊断周期
#define  EXTH_PUMP_FAULT_PERIOD  100
//启泵诊断通过持续时间                                  
#define  EXTH_PUMP_FUALT_PASS    100
//启泵诊断失败持续时间                                                                     
#define  EXTH_PUMP_FAULT_FAILED	 5000
//启泵诊断通过次数                                                           
#define  EXTH_PUMP_FAULT_PASS_LIMIT    (EXTH_PUMP_FUALT_PASS / EXTH_PUMP_FAULT_PERIOD)      
//启泵诊断失败次数  
#define  EXTH_PUMP_FAULT_FAILED_LIMIT  (EXTH_PUMP_FAULT_FAILED / EXTH_PUMP_FAULT_PERIOD)  

//刺破阀诊断周期
#define  EXTH_MAIN_FAULT_PERIOD 100
//刺破阀诊断通过持续时间                                  
#define  EXTH_MAIN_FUALT_PASS   100
//刺破阀诊断失败持续时间                                                                     
#define  EXTH_MAIN_FAULT_FAILED 2000
//刺破阀诊断通过次数                                                           
#define  EXTH_MAIN_FAULT_PASS_LIMIT (EXTH_MAIN_FUALT_PASS / EXTH_MAIN_FAULT_PERIOD)      
//刺破阀诊断失败次数  
#define  EXTH_MAIN_FAULT_FAILED_LIMIT (EXTH_MAIN_FAULT_FAILED / EXTH_MAIN_FAULT_PERIOD)  

//球阀诊断周期
#define  EXTH_BALL_FAULT_PERIOD  100
//球阀诊断通过持续时间                                  
#define  EXTH_BALL_FUALT_PASS   100
//球阀诊断失败持续时间                                                                     
#define  EXTH_BALL_FAULT_FAILED   2000
//球阀诊断通过次数                                                           
#define  EXTH_BALL_FAULT_PASS_LIMIT (EXTH_BALL_FUALT_PASS/EXTH_BALL_FAULT_PERIOD)      
//球阀诊断失败次数  
#define  EXTH_BALL_FAULT_FAILED_LIMIT (EXTH_BALL_FAULT_FAILED / EXTH_BALL_FAULT_PERIOD)  



//刺破阀异常诊断周期
#define  EXTH_MAIN_ABNORMAL_FAULT_PERIOD  100
//刺破阀异常通过持续时间                                  
#define  EXTH_MAIN_ABNORMAL_FUALT_PASS   2000
//刺破阀异常失败持续时间                                                                     
#define  EXTH_MAIN_ABNORMAL_FAULT_FAILED     2000
//刺破阀异常通过次数                                                           
#define  EXTH_MAIN_ABNORMAL_FAULT_PASS_LIMIT (EXTH_MAIN_ABNORMAL_FUALT_PASS/EXTH_MAIN_ABNORMAL_FAULT_PERIOD)      
//刺破阀异常失败次数  
#define  EXTH_MAIN_ABNORMAL_FAULT_FAILED_LIMIT (EXTH_MAIN_ABNORMAL_FAULT_FAILED / EXTH_MAIN_ABNORMAL_FAULT_PERIOD)  



//球阀异常诊断周期
#define  EXTH_BALL_ABNORMAL_FUALT_PERIOD  100
//球阀异常通过持续时间                                  
#define  EXTH_BALL_ABNORMAL_FUALT_PASS   2000
//球阀异常失败持续时间                                                                     
#define  EXTH_BALL_ABNORMAL_FAULT_FAILED   2000
//球阀异常通过次数                                                           
#define  EXTH_BALL_ABNORMAL_FAULT_PASS_LIMIT (EXTH_BALL_ABNORMAL_FUALT_PASS/EXTH_BALL_ABNORMAL_FUALT_PERIOD)      
//球阀异常失败次数  
#define  EXTH_BALL_ABNORMAL_FAULT_FAILED_LIMIT (EXTH_BALL_ABNORMAL_FAULT_FAILED / EXTH_BALL_ABNORMAL_FUALT_PERIOD)  

void diagNormalExthNodeInit(void);
void DiagPumpSelfcheckStart(void);

void DiagPumpsStart(void);
void DiagPierceValStart(void);
void DiagBallValStart(void);
#endif

