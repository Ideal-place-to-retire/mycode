/**
 * @file exth_conf.h
 * @author your name (you@domain.com)
 * @brief 灭火器配置信息数据结构
 * @version 0.1
 * @date 2023-08-30
 * 
 * @copyright Copyright (c) 2023
 * 
 */


#ifndef EXTH_CONF_H
#define EXTH_CONF_H


//#include "Bsp_Bus.h"
#include "stdint.h"


/*********灭火器默认配置信息，如果不满足项目需求 需要进行更改***************************************/

// 灭火器索引，当前最大配置数据 2台
typedef enum
{
	EXTH_1 = 0X00,
	EXTH_2 = 0X01,
	EXTH_MAX,
}ENU_EXTH_INDEX;


// 灭火器装置泵配置
typedef enum
{
	EXTH_PUMP_1 = 0X00,
	EXTH_PUMP_2 = 0X01,
	
	
	EXTH_PUMP_MAX,
}ENU_EXTH_PUMP_INDEX;


// 灭火器装置泵配置
typedef enum
{
	PIERCE_VALVE = 0X00,
	BALL_VALVE = 0X01,
	
	
	EXTH_VALVE_MAX,
}ENU_EXTH_VALVE_INDEX;



// 一级分阀最大数量
#define SUB_VALVE_MAX 10
//每个分阀下挂的选择阀个数
#define SELECT_VAL_MAX_PER_SUB  8

// 
#define CONFG_NONE  0XFF      

/************************************************/

typedef enum
{
    SUB_VALVE_L1,     // 一级分阀
    SELECT_VALE_L2,     // 二级选择阀

}ENU_VALVE_TYPE;//请求启动阀门组


typedef struct 
{
     ENU_VALVE_TYPE     valve_type; // 请求启动的阀门级别：一级分阀、或者 二级选择阀
     uint8_t *          valves_index;      // 请求启动的阀门索引指针
     uint8_t            valves_number;     // 请求启动的阀门数量

}STR_REQUEST_VAL_GROUP;         //请求启动阀门组


/////////////////灭火器配置/////////////////////////////////////

typedef struct {
    uint8_t                 Exth_Index;                 // 灭火器索引，防护区根据灭火器的索引选择灭火器进行启动,0XFF = no config
    const char              *ExthName;                  // 灭火器名称
    uint16_t                Exth_Code;                  // 灭火装置编号  

    uint8_t             st_MainValve_Pos[EXTH_VALVE_MAX];                //主阀位置
    uint8_t             st_MainPump_Pos[EXTH_PUMP_MAX];                 //主泵位置   
     
    void (*ExthRequestIfr)(void *exth_Ptr, uint8_t release_policy_index, void *st_request_valve_group); // 灭火器请求启动阀门           

} STR_CFG_EXTH;










#endif



