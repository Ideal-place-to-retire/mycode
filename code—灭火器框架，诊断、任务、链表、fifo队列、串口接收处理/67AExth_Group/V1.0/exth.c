/**
 * @file exth.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-29
 *
 * @copyright Copyright (c) 2023
 * 
 */


#include "exth.h"
#include "Proj_exth_conf.h"
#include "stddef.h"



STR_EXTH_POINT   exth_point_group[EXTH_MAX];




void ExthReset(STR_EXTH_POINT *exthPtr)
{
    // 复位标志位 
    exthPtr->IsExthStartFlag = false;
    exthPtr->ExthStatus = EXTH_IDEL;

    exthPtr->LiquidLevel    = 0;
    exthPtr->PressureValue  = 0;
    exthPtr->ReleaseCount   = 0;
    exthPtr->RunTime        = 0;

    for(int pump_num = 0; pump_num < EXTH_PUMP_MAX; pump_num++)
    {
        exthPtr->st_MainPump[pump_num].Device_FeedBack = DIAG_NONE;
        exthPtr->st_MainPump[pump_num].Device_Status   = EXTH_IDEL;
    }

    for(int valve_num = 0; valve_num < EXTH_VALVE_MAX; valve_num++)
    {
        exthPtr->st_MainValve[valve_num].Device_FeedBack = DIAG_NONE;
        exthPtr->st_MainValve[valve_num].Device_Status   = EXTH_IDEL;

    }
}

void ExthInit(const STR_CFG_EXTH *exth_CfgPtr, uint8_t exth_cfg_num)
{
    //c_bzero(exth_point_group, sizeof(exth_point_group));

    for(int i = 0; i < EXTH_MAX ; i++)
    {
        if(exth_CfgPtr[i].Exth_Index != CONFG_NONE)
        {
            exth_point_group[i].Exth_Index = exth_CfgPtr[i].Exth_Index;
            exth_point_group[i].Exth_Code  = exth_CfgPtr[i].Exth_Code;
            
            for(int pump_num = 0; pump_num < EXTH_PUMP_MAX; pump_num++)
            {
                exth_point_group[i].st_MainPump[pump_num].Device_BusPos = exth_CfgPtr[i].st_MainPump_Pos[pump_num];
            }

            for(int valve_num = 0; valve_num < EXTH_VALVE_MAX; valve_num++)
            {
                exth_point_group[i].st_MainValve[valve_num].Device_BusPos = exth_CfgPtr[i].st_MainValve_Pos[valve_num];

            }

            exth_point_group[i].ExthRequestIfr = exth_CfgPtr[i].ExthRequestIfr;
            // if(exth_point_group[i].ExthRequestIfr != NULL)
            // {
            //     printf("-----------request is copnnect---------------------\n");

            // }
            /******debug*****************/
            printf("--------------now exth is %d ------------------------\n",i);
            printf("Exth_Index is %d \n",exth_point_group[i].Exth_Index);
            printf("Exth_Code is %d \n",exth_point_group[i].Exth_Code);
            printf("Device_BusPos is %d \n",exth_point_group[i].st_MainPump[i].Device_BusPos);
            printf("Device_BusPos is %d \n",exth_point_group[i].st_MainValve[i].Device_BusPos);
            //printf("st_BallValve.Device_BusPos is %d \n",exth_point_group[i].st_BallValve.Device_BusPos);
            /*******************/
        }
        else
        {
            exth_point_group[i].Exth_Index = exth_CfgPtr[i].Exth_Index;
             printf("\n----++++++++++++++++++++exth is not config %d ------------------------\n",i);
        }
        

    }

}


void ExthRequest(uint8_t exth_index, uint8_t release_policy_index, void *st_valve_group)
{
    printf("exth rueqest :\n ");
    if(exth_index > EXTH_MAX )
    {
        return;
    }
    if(exth_point_group[exth_index].ExthRequestIfr != NULL)
    {
         printf(" request  projexth request\n ");
         // 请求指定灭火器执行喷洒策略
        exth_point_group[exth_index].ExthRequestIfr(&exth_point_group[exth_index], release_policy_index, st_valve_group);
        // 设置指定灭火器状态为 启动中……
        exth_point_group[exth_index].IsExthStartFlag = true;
        // TO DO: 保存当前事件：灭火器请求启动

    }
    else
    {
        printf("exth request func is not connect \n");
    }
}



void ExthMangeCtr()
{
    uint8_t i;

   // STR_EXTH_POINT *exthPtr = exth_point_group;

    for(i = 0; i < EXTH_MAX; i++ )
    {
        printf("now mange exth is %d \n", i);
        if( exth_point_group[i].ReleasePolicGroupIfr != NULL)
        {
            printf("exth policy connect \n");
            exth_point_group[i].ReleasePolicGroupIfr(&exth_point_group[i]);
        }
        else
        {
            printf("exth policy is not connect \n");
        }      
    }
}


