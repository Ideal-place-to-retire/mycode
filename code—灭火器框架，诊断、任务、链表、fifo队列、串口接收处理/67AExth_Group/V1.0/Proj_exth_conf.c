/**
 * @file Proj_exth_conf.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-08-29
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "Proj_exth_conf.h"
#include "stddef.h"//NULL
#include "exth.h"


#define BUS_WRITE(POS, OP)   printf("Device :%d do %d  \n", POS,  OP)


static void SetValveState(STR_EXTH_POINT *exth_Ptr, STR_REQUEST_VAL_GROUP *st_valve_group);

void ProjExthRequest(void *exth_Ptr, uint8_t release_policy_index, void *st_valve_group);

// 喷洒策略组声明
void ReleasePolicy1(void *exth_Ptr);





#define PUMP_OUT1_POS       4
#define PUMP_OUT2_POS       14
#define BALLVALVE_OUT_POS   28
#define PIERCE_OUT_POS      30
    

#define PIERCE_VALVE       0  
#define BALL_VALVE         1 



/**
 * @brief 项目中 灭火器配置信息
 * 
 */
const STR_CFG_EXTH stExthCfgTable[EXTH_MAX] = {
    [0] = 
    {
        .Exth_Index = 1,
        .ExthName = "全氟己酮",
        .Exth_Code  = 0xEE01,
        .st_MainPump_Pos = 
        {
            PUMP_OUT1_POS,
            PUMP_OUT2_POS,
        },
    
        .st_MainValve_Pos = 
        {
            PIERCE_OUT_POS,         // 刺破阀
            BALLVALVE_OUT_POS       // 球阀
        },
               
        .ExthRequestIfr = ProjExthRequest,     
    },
    [1] =
    {
        .Exth_Index = CONFG_NONE,
    }
};



static const uint8_t ExthCfgNbr = sizeof(stExthCfgTable)/sizeof(stExthCfgTable[0]);





/**
 * @brief 灭火器请求启动
 * 
 * @param exthPtr            请求启动的灭火器节点
 * @param release_policy_index  喷洒策略索引
 * @param st_valve_group        use struct point: STR_REQUEST_VAL_GROUP 本次请求启动的终端阀门类型、索引、数量
 */
void ProjExthRequest(void *exth_Ptr, uint8_t release_policy_index, void *st_valve_group)
{
   
    STR_EXTH_POINT *this_exth_ptr = (STR_EXTH_POINT *) exth_Ptr;
    STR_REQUEST_VAL_GROUP * val_group = (STR_REQUEST_VAL_GROUP *)st_valve_group;

    printf("Proj exth requeset  policy %d set \n",release_policy_index);

    //装载 喷洒策略组函数
    switch (release_policy_index)
    {
    case 1:
         this_exth_ptr->ReleasePolicGroupIfr = ReleasePolicy1;
        break;
    default:
        this_exth_ptr->ReleasePolicGroupIfr = ReleasePolicy1;
        break;
    }

    // 切换灭火器设置 分阀状态
     SetValveState(exth_Ptr, val_group);

         printf(" request set is ok \n");

}



typedef struct 
{
    uint16_t ReleaseTime;
    uint16_t StopTime;
}Exth_RELEASE_POLICY;

Exth_RELEASE_POLICY PolictTable[] =
{
    {130, 30},
    {100, 30},
    {100, 0 },
};

const uint8_t RELEASE_NUM = sizeof(PolictTable)/sizeof(PolictTable[0]);


// 切换阀门状态
void MainValveStatueChange(STR_EXTH_POINT *this_exthPtr, ENU_EXTH_DEV_STATUS  status)
{
    this_exthPtr->st_MainValve[PIERCE_VALVE].Device_Status = status;
    this_exthPtr->st_MainValve[BALL_VALVE].Device_Status = status;
}

// 切换泵组状态
void MainPumpStatueChange(STR_EXTH_POINT *this_exthPtr, ENU_EXTH_DEV_STATUS  status)
{
    this_exthPtr->st_MainPump[0].Device_Status = status;
    this_exthPtr->st_MainPump[1].Device_Status = status;
}

// 泵组开启关闭
void PumpsOperate(STR_EXTH_POINT *this_exthPtr, uint8_t op)
{
    BUS_WRITE(this_exthPtr->st_MainPump[0].Device_BusPos, op ); //启动泵
    BUS_WRITE(this_exthPtr->st_MainPump[1].Device_BusPos, op ); //启动泵
}

// 阀门开启关闭
void ValvesOperate(STR_EXTH_POINT *this_exthPtr, uint8_t op)
{
    BUS_WRITE(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_BusPos, op );// 拉高 刺破阀输出
    BUS_WRITE(this_exthPtr->st_MainValve[BALL_VALVE].Device_BusPos, op ); 
}




/**
 * @brief 喷洒策略组1
 * 
 */
void ReleasePolicy1(void *exth_Ptr)
{
     STR_EXTH_POINT *this_exthPtr = (STR_EXTH_POINT *) exth_Ptr;

    printf("policy1 is in \n");
    if(this_exthPtr->IsExthStartFlag == true)
    {
        switch (this_exthPtr->ExthStatus)
        {
            case EXTH_IDEL:
                break;
            case EXTH_REQUEST:
                 printf("policy1  is start  \n");
                /*request 中设置 两个阀门状态为请求 */
                if( this_exthPtr->st_MainValve[PIERCE_VALVE].Device_Status == EXTH_DEV_IDEL && this_exthPtr->st_MainValve[BALL_VALVE].Device_Status == EXTH_DEV_IDEL)
                {
                    ValvesOperate(this_exthPtr, 1);
                    MainValveStatueChange(this_exthPtr, EXTH_DEV_REQUEST);                   
                    
                    printf("policy1  in open value  \n");
                    // TODO: 保存当前事件：启动刺破阀、启动球阀

                    // TODO: 装载 刺破阀反馈诊断函数

                    return;
                }
                /*装载诊断函数，两个阀门的反馈都正常，设置泵是请求状态，并启动泵 装载诊断 */
                if(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_FeedBack == DIAG_PASS && this_exthPtr->st_MainValve[BALL_VALVE].Device_FeedBack == DIAG_PASS)
                {
                    PumpsOperate(this_exthPtr, 1); //启动泵

                    MainValveStatueChange(this_exthPtr, EXTH_DEV_OPEN);  // change valve status to open

                    MainPumpStatueChange(this_exthPtr, EXTH_DEV_REQUEST);  // change pumps status to request

                    // TODO: 保存当前事件：启动泵

                    // TODO: 装载 泵反馈诊断函数

                    return;
                }
                else if(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_FeedBack ==DIAG_FAIL || this_exthPtr->st_MainValve[BALL_VALVE].Device_FeedBack == DIAG_FAIL)
                {
                    if(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_FeedBack == DIAG_FAIL )
                    {                        
                        //TODO：保存当前事件： 刺破阀故障关闭
                    }

                    if(this_exthPtr->st_MainValve[BALL_VALVE].Device_FeedBack == DIAG_FAIL)
                    {    
                        //TODO： 保存当前事件： 球阀故障关闭
                    }
                    ValvesOperate(this_exthPtr, 0);

                    this_exthPtr->ExthStatus = EXTH_FAILED;
                }

                /*检测到 泵的诊断是通过的，设置泵的状态为开始，并开始倒计时控制 */
                if(this_exthPtr->st_MainPump[0].Device_FeedBack == DIAG_PASS && this_exthPtr->st_MainPump[1].Device_FeedBack == DIAG_PASS)
                {
                    MainPumpStatueChange(this_exthPtr, EXTH_DEV_OPEN); // pumps status change to open
                    this_exthPtr->RunTime = PolictTable[this_exthPtr->ReleaseCount].ReleaseTime;
                    this_exthPtr->ExthStatus = EXTH_START;  //exth changes to next step
                    // TODO: 保存当前事件： 喷洒开始
                    return;
                }
                else if(this_exthPtr->st_MainPump[0].Device_FeedBack == DIAG_FAIL || this_exthPtr->st_MainPump[1].Device_FeedBack == DIAG_FAIL)
                {
                    ValvesOperate(this_exthPtr, 0);// 拉低 关闭刺破阀输出\关闭球阀输出
                    PumpsOperate(this_exthPtr, 0); //启动泵 // 拉低 关闭球阀输出
                        
                    this_exthPtr->ExthStatus = EXTH_FAILED;
                    //TODO： 保存当前事件： 泵故障关闭
                }
                break;

            case EXTH_START:
                /*泵开始启动，执行喷洒循环*/
                    if(this_exthPtr->RunTime == 0)
                    {
                        this_exthPtr->RunTime = PolictTable[this_exthPtr->ReleaseCount].StopTime;

                        MainPumpStatueChange(this_exthPtr, EXTH_DEV_STOP); 
                        PumpsOperate(this_exthPtr, 0); //关闭泵

                        this_exthPtr->ExthStatus = EXTH_WAIT;
                        // TODO: 保存当前事件： 喷洒暂停
                        
                    }
                    else
                    {
                        this_exthPtr->RunTime--;
                    }

                    return;
                break;
            
            case EXTH_WAIT:
                /*灭火器 停止喷洒*/               
                    if(this_exthPtr->RunTime == 0)
                    {
                        this_exthPtr->ReleaseCount++;
                                                
                        if(this_exthPtr->ReleaseCount > RELEASE_NUM)
                        {
                            MainPumpStatueChange(this_exthPtr, EXTH_DEV_FINI); 
                            MainValveStatueChange(this_exthPtr, EXTH_DEV_FINI); 

                            this_exthPtr->ExthStatus = EXTH_SUCC;

                            ValvesOperate(this_exthPtr, 0);// 拉低 关闭刺破阀输出\关闭球阀输出
                            PumpsOperate(this_exthPtr, 0); //启动泵 // 拉低 关闭球阀输出

                            // TODO: 保存当前事件： 喷洒结束
                            return; 
                        }

                        this_exthPtr->RunTime = PolictTable[ this_exthPtr->ReleaseCount ].ReleaseTime;
                        MainPumpStatueChange(this_exthPtr, EXTH_DEV_OPEN); 
                        PumpsOperate(this_exthPtr, 1); // 打开泵
                        // TODO: 保存当前事件： 喷洒开始
                    }
                    else
                    {
                        this_exthPtr->RunTime--;
                    }                   
                    return;
                
                break;


            case EXTH_SUCC:
                break;
            case EXTH_FAILED:
                break;

            default:
                break;
        }
    }
}








static void SetValveState(STR_EXTH_POINT *exth_Ptr, STR_REQUEST_VAL_GROUP *st_valve_group)
{
    STR_EXTH_POINT *this_exth_ptr = (STR_EXTH_POINT *) exth_Ptr;

    this_exth_ptr->ExthStatus = EXTH_REQUEST;
}




void PorjExthInit()
{
    ExthInit(stExthCfgTable, ExthCfgNbr);
}



int main()
{
    int i = 20;
    PorjExthInit();

    ExthRequest(0, 1, NULL);
   
    while (i)
    {
       i--;
        ExthMangeCtr();
    }
    
   
}





