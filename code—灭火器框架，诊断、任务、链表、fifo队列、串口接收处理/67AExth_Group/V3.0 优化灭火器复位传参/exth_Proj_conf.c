/**
 * @file Proj_exth_conf.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-08-29
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "exth_Proj_conf.h"
#include "stddef.h"//NULL
#include "exth.h"
#include "event_queue_Handle.h"
#include "exth_diage_node.h"
#include "MidDio.h"


#include "SEGGER_RTT.h"


#define BUS_WRITE(POS, OP)   DioWriteChannel(POS,  OP)


static void SetValveState(STR_EXTH_POINT *exth_Ptr, STR_REQUEST_VAL_GROUP *st_valve_group);

void ProjExthRequest(void *exth_Ptr, uint8_t release_policy_index, void *st_valve_group);

// ��������������
void ReleasePolicy1(void *exth_Ptr);





/**
 * @brief ��Ŀ�� �����������Ϣ
 * 
 */
const STR_CFG_EXTH stExthCfgTable[EXTH_MAX] = {
    [0] = 
    {
        .Exth_Index = EXTH_1,
        .ExthName = "ȫ����ͪ",
        .Exth_Code  = 0xEE01,
        .st_MainPump_Pos = 
        {
            PUMP1_OPEN,
            PUMP2_OPEN,
        },
    
        .st_MainValve_Pos = 
        {
            PIERCE_VALVE_OPEN,         // ���Ʒ�
            BALL_VALVE_OPEN       // ��
        },
               
        .ExthRequestIfr = ProjExthRequest,     
    },
//    [1] =
//    {
//        .Exth_Index = CONFG_NONE,
//    }
};



static const uint8_t ExthCfgNbr = sizeof(stExthCfgTable)/sizeof(stExthCfgTable[0]);





/**
 * @brief �������������
 * 
 * @param exthPtr            ����������������ڵ�
 * @param release_policy_index  ������������
 * @param st_valve_group        use struct point: STR_REQUEST_VAL_GROUP ���������������ն˷������͡�����������
 */
void ProjExthRequest(void *exth_Ptr, uint8_t release_policy_index, void *st_valve_group)
{
   
    STR_EXTH_POINT *this_exth_ptr = (STR_EXTH_POINT *) exth_Ptr;
    STR_REQUEST_VAL_GROUP * val_group = (STR_REQUEST_VAL_GROUP *)st_valve_group;

	DEBUG_LOG_INFO(DEBUG_LEVEL_2, "Proj exth requeset  policy %d set \n",release_policy_index);
    printf("Proj exth requeset  policy %d set \n",release_policy_index);

    //װ�� ���������麯��
    switch (release_policy_index)
    {
		case STRATEGY_1:
			 this_exth_ptr->ReleasePolicGroupIfr = ReleasePolicy1;
			break;
		default:
			this_exth_ptr->ReleasePolicGroupIfr = ReleasePolicy1;
			break;
    }

    // �л���������� �ַ�״̬
     SetValveState(exth_Ptr, val_group);

         printf(" request set is ok \n");

}



typedef struct 
{
    uint16_t ReleaseTime;
    uint16_t StopTime;
}Exth_RELEASE_POLICY;



#define RELEASE_TIME(x)	 (x/EXTH_CTR_PERIOD_MS)
#define PURSE_TIME(x)	 (x/EXTH_CTR_PERIOD_MS)

Exth_RELEASE_POLICY PolictTable[] =
{
    {RELEASE_TIME(130), PURSE_TIME(30)},
    {RELEASE_TIME(100), PURSE_TIME(30)},
	{RELEASE_TIME(100), PURSE_TIME(0)},    
	
//	{20, 10},
//	{30, 5},
//	{15, 0 },
	
};

const uint8_t RELEASE_NUM = sizeof(PolictTable)/sizeof(PolictTable[0]);


// �л�����״̬
static void MainValveStatueChange(STR_EXTH_POINT *this_exthPtr, ENU_EXTH_DEV_STATUS  status)
{
    this_exthPtr->st_MainValve[PIERCE_VALVE].Device_Status = status;
    this_exthPtr->st_MainValve[BALL_VALVE].Device_Status = status;
}

// �л�����״̬
static void MainPumpStatueChange(STR_EXTH_POINT *this_exthPtr, ENU_EXTH_DEV_STATUS  status)
{
    this_exthPtr->st_MainPump[EXTH_PUMP_1].Device_Status = status;
    this_exthPtr->st_MainPump[EXTH_PUMP_2].Device_Status = status;
}

// ���鿪���ر�
static void PumpsOperate(STR_EXTH_POINT *this_exthPtr, uint8_t op)
{
    BUS_WRITE(this_exthPtr->st_MainPump[EXTH_PUMP_1].Device_BusPos, op ); //������
    BUS_WRITE(this_exthPtr->st_MainPump[EXTH_PUMP_2].Device_BusPos, op ); //������
}

// ���ſ����ر�
static void ValvesOperate(STR_EXTH_POINT *this_exthPtr, uint8_t op)
{
    BUS_WRITE(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_BusPos, op );// ���� ���Ʒ����
    BUS_WRITE(this_exthPtr->st_MainValve[BALL_VALVE].Device_BusPos, op ); 
}




/**
 * @brief ����������1
 * 
 */
void ReleasePolicy1(void *exth_Ptr)
{
    STR_EXTH_POINT *this_exthPtr = (STR_EXTH_POINT *) exth_Ptr;

    printf("policy1 is in \n");
	DEBUG_LOG_INFO(DEBUG_LEVEL_2, "policy1 is in \n");

	 /*request ������ �����������λ */
    if(this_exthPtr->IsExthStartFlag == true)
    {		
        switch (this_exthPtr->ExthStatus)
        {
            case EXTH_IDEL:
                break;
            case EXTH_REQUEST:
//              printf("policy1  is start  \n");
//				DEBUG_LOG_INFO(DEBUG_LEVEL_2, "policy1  is start  \n");
			
                if( this_exthPtr->st_MainValve[PIERCE_VALVE].Device_Status == EXTH_DEV_IDEL && \
					this_exthPtr->st_MainValve[BALL_VALVE].Device_Status == EXTH_DEV_IDEL)
                {
                    ValvesOperate(this_exthPtr, 1);							// �������Ʒ�����
                    MainValveStatueChange(this_exthPtr, EXTH_DEV_REQUEST);  // �л����Ʒ������豸״̬             
                    
//                    printf("policy1  in open value  \n");
//					DEBUG_LOG_INFO(DEBUG_LEVEL_2, "policy1  in open value  \n");
                    
					/* ���浱ǰ�¼����������Ʒ��������� */
					EvtExthWrite(EXTH_MAINVAL_OPEN_EVT);
					EvtExthWrite(EXTH_BALLVAL_OPEN_EVT);
					
                    /* װ�� ���Ʒ�������Ϻ��� */
					DiagPierceValStart();
					DiagBallValStart();
										
                    return;
                }
                
				if( this_exthPtr->st_MainPump[EXTH_PUMP_1].Device_Status == EXTH_DEV_IDEL && \
					this_exthPtr->st_MainPump[EXTH_PUMP_2].Device_Status == EXTH_DEV_IDEL)
				{
					/*װ����Ϻ������������ŵķ��������������ñ�������״̬���������� װ����� */
					if(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_FeedBack == DIAG_PASS && this_exthPtr->st_MainValve[BALL_VALVE].Device_FeedBack == DIAG_PASS)
					{
						PumpsOperate(this_exthPtr, 1); 							//������

						MainValveStatueChange(this_exthPtr, EXTH_DEV_OPEN);  	// change valve status to EXTH_DEV_OPEN

						MainPumpStatueChange(this_exthPtr, EXTH_DEV_REQUEST);  // change pumps status to EXTH_DEV_REQUEST

						// ���浱ǰ�¼���������
						EvtExthWrite(EXTH_PUMP_OPEN_EVT);

						// TODO: װ�� �÷�����Ϻ���
						DiagPumpsStart();
						DEBUG_LOG_INFO(DEBUG_LEVEL_2, " open pump ------------ \n");

						return;
					}
					else if(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_FeedBack ==DIAG_FAIL || this_exthPtr->st_MainValve[BALL_VALVE].Device_FeedBack == DIAG_FAIL)
					{
						if(this_exthPtr->st_MainValve[PIERCE_VALVE].Device_FeedBack == DIAG_FAIL )
						{                        
							/* ���浱ǰ�¼��� ���Ʒ����Ϲر� */
							EvtExthWrite(EXTH_MAINVAL_FAULT_CLOSE_EVT);
							this_exthPtr->st_MainValve[PIERCE_VALVE].Device_Status = EXTH_DEV_FAULT;							
						}

						if(this_exthPtr->st_MainValve[BALL_VALVE].Device_FeedBack == DIAG_FAIL)
						{    
							/* ���浱ǰ�¼��� �򷧹��Ϲر� */
							EvtExthWrite(EXTH_BALLVAL_FAULT_CLOSE_EVT);
							this_exthPtr->st_MainValve[BALL_VALVE].Device_Status = EXTH_DEV_FAULT;
						}
						
						ValvesOperate(this_exthPtr, 0);

						this_exthPtr->ExthStatus = EXTH_FAILED;
					}			
				}
				
				if( this_exthPtr->st_MainPump[EXTH_PUMP_1].Device_Status == EXTH_DEV_REQUEST && \
					this_exthPtr->st_MainPump[EXTH_PUMP_2].Device_Status == EXTH_DEV_REQUEST)
				{
					/*��⵽ �õ������ͨ���ģ����ñõ�״̬Ϊ��ʼ������ʼ����ʱ���� */
					if(this_exthPtr->st_MainPump[EXTH_PUMP_1].Device_FeedBack == DIAG_PASS && this_exthPtr->st_MainPump[EXTH_PUMP_2].Device_FeedBack == DIAG_PASS)
					{
						/* pumps status change to open */
						MainPumpStatueChange(this_exthPtr, EXTH_DEV_OPEN); 		
						
						this_exthPtr->RunTime = PolictTable[this_exthPtr->ReleaseCount].ReleaseTime;
						
						/* exth changes to next step */
						this_exthPtr->ExthStatus = EXTH_START;  				
						
						/* ���浱ǰ�¼��� ������ʼ */
						EvtExthWrite(EXTH_RELEASE_START_EVT);
											
						DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is start �������� \n");

						return;
					}
					else if(this_exthPtr->st_MainPump[0].Device_FeedBack == DIAG_FAIL || this_exthPtr->st_MainPump[1].Device_FeedBack == DIAG_FAIL)
					{
						/* �л� �õ�״̬Ϊ ���� */
						MainPumpStatueChange(this_exthPtr, EXTH_DEV_FAULT); 
						
						/* ���� �رմ��Ʒ����\�ر������ */
						ValvesOperate(this_exthPtr, 0);
						
						/* �رձ� */ 
						PumpsOperate(this_exthPtr, 0); 
							
						this_exthPtr->ExthStatus = EXTH_FAILED;
						
						/* ���浱ǰ�¼��� �ù��Ϲر� */
						EvtExthWrite(EXTH_PUMP_FAULT_CLOSE_EVT);						
					}		
				}
                
				
                break;

            case EXTH_START:
                /*�ÿ�ʼ������ִ������ѭ��*/
                    if(this_exthPtr->RunTime == 0)
                    {
                        this_exthPtr->RunTime = PolictTable[this_exthPtr->ReleaseCount].StopTime;

                        MainPumpStatueChange(this_exthPtr, EXTH_DEV_STOP); 
                        PumpsOperate(this_exthPtr, 0); //�رձ�

                        this_exthPtr->ExthStatus = EXTH_WAIT;
                        // TODO: ���浱ǰ�¼��� ������ͣ
                        EvtExthWrite(EXTH_RELEASE_PAUSE_EVT);
						
					DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is stop  now \n");

                    }
                    else
                    {
                        this_exthPtr->RunTime--;
                    }
					
					DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is start now time is %d  \n",this_exthPtr->RunTime);

                   
                break;
            
            case EXTH_WAIT:
                /*����� ֹͣ����*/               
                    if(this_exthPtr->RunTime == 0)
                    {
                        this_exthPtr->ReleaseCount++;
						
//                        DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is in stop  now count is %d \n",this_exthPtr->ReleaseCount);
                      
                        if(this_exthPtr->ReleaseCount >= RELEASE_NUM)
                        {
                            MainPumpStatueChange(this_exthPtr, EXTH_DEV_FINI); 
                            MainValveStatueChange(this_exthPtr, EXTH_DEV_FINI); 

                            this_exthPtr->ExthStatus = EXTH_SUCC;

                            ValvesOperate(this_exthPtr, 0);// ���� �رմ��Ʒ����\�ر������
                            PumpsOperate(this_exthPtr, 0); //������ // ���� �ر������

                            // TODO: ���浱ǰ�¼��� ��������
							EvtExthWrite(EXTH_RELEASE_OVER_EVT);
							
							
							DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is overrrrrr  \n");
 				
                            return; 
                        }

                        this_exthPtr->RunTime = PolictTable[ this_exthPtr->ReleaseCount ].ReleaseTime;
						this_exthPtr->ExthStatus = EXTH_START;
						
                        MainPumpStatueChange(this_exthPtr, EXTH_DEV_OPEN); 
                        PumpsOperate(this_exthPtr, 1); // �򿪱�
                        
						// TODO: ���浱ǰ�¼��� ������ʼ
						EvtExthWrite(EXTH_RELEASE_START_EVT);
						
//						DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is start aginnnnnnnnnnn now time is %d  \n",this_exthPtr->RunTime);
                    }
                    else
                    {
                        this_exthPtr->RunTime--;
					
						DEBUG_LOG_INFO(DEBUG_LEVEL_2, " release is stoppppp now time is %d  \n",this_exthPtr->RunTime);

                    } 
					
                break;


            case EXTH_SUCC:
                break;
            case EXTH_FAILED:
                break;

            default:
                break;
        }
    }
}








static void SetValveState(STR_EXTH_POINT *exth_Ptr, STR_REQUEST_VAL_GROUP *st_valve_group)
{
    STR_EXTH_POINT *this_exth_ptr = (STR_EXTH_POINT *) exth_Ptr;

    this_exth_ptr->ExthStatus = EXTH_REQUEST;
}




void PorjExthInit()
{
    ExthInit(stExthCfgTable, ExthCfgNbr);
}







