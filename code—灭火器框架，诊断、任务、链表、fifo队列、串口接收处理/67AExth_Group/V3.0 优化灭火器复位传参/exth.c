/**
 * @file exth.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-29
 *
 * @copyright Copyright (c) 2023
 * 
 */


#include "exth.h"
#include "exth_Proj_conf.h"
#include "stddef.h"
#include "c_mem.h"
#include "event_queue_Handle.h"




#include "SEGGER_RTT.h"


STR_EXTH_POINT   exth_point_group[EXTH_MAX];




void ExthReset(ENU_EXTH_INDEX exth_index)
{	
	if(exth_index >= EXTH_MAX)
	{
		return;
	}
	
	STR_EXTH_POINT *exthPtr = &exth_point_group[exth_index];
	
    // 复位标志位 
    exthPtr->IsExthStartFlag = false;
    exthPtr->ExthStatus = EXTH_IDEL;

    exthPtr->LiquidLevel    = 0;
    exthPtr->PressureValue  = 0;
    exthPtr->ReleaseCount   = 0;
    exthPtr->RunTime        = 0;

    for(int pump_num = 0; pump_num < EXTH_PUMP_MAX; pump_num++)
    {
        exthPtr->st_MainPump[pump_num].Device_FeedBack = DIAG_NONE;
        exthPtr->st_MainPump[pump_num].Device_Status   = EXTH_DEV_IDEL;
    }

    for(int valve_num = 0; valve_num < EXTH_VALVE_MAX; valve_num++)
    {
        exthPtr->st_MainValve[valve_num].Device_FeedBack = DIAG_NONE;
        exthPtr->st_MainValve[valve_num].Device_Status   = EXTH_DEV_IDEL;

    }
}

void ExthInit(const STR_CFG_EXTH *exth_CfgPtr, uint8_t exth_cfg_num)
{
    c_bzero(exth_point_group, sizeof(exth_point_group));

    for(int i = 0; i < EXTH_MAX ; i++)
    {       
            exth_point_group[i].Exth_Index = exth_CfgPtr[i].Exth_Index;
            exth_point_group[i].Exth_Code  = exth_CfgPtr[i].Exth_Code;
            
            for(int pump_num = 0; pump_num < EXTH_PUMP_MAX; pump_num++)
            {
                exth_point_group[i].st_MainPump[pump_num].Device_BusPos = exth_CfgPtr[i].st_MainPump_Pos[pump_num];
            }

            for(int valve_num = 0; valve_num < EXTH_VALVE_MAX; valve_num++)
            {
                exth_point_group[i].st_MainValve[valve_num].Device_BusPos = exth_CfgPtr[i].st_MainValve_Pos[valve_num];

            }

            exth_point_group[i].ExthRequestIfr = exth_CfgPtr[i].ExthRequestIfr;  

//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " exth_point_group[i].st_MainPump[0].Device_BusPos %d\n ", exth_point_group[i].st_MainPump[1].Device_BusPos);
//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " exth_point_group[i].st_MainValve[0].Device_BusPos %d\n ", exth_point_group[i].st_MainValve[1].Device_BusPos);
     
    }

}


void ExthRequest(ENU_EXTH_INDEX exth_index, ENU_STRATEGY_INDEX release_policy_index, void *st_valve_group)
{
    printf("exth rueqest :\n ");
	DEBUG_LOG_INFO(DEBUG_LEVEL_2, "exth rueqest :\n ");
	
    if(exth_index > EXTH_MAX )
    {
        return;
    }
	
    if(exth_point_group[exth_index].ExthRequestIfr != NULL)
    {
        printf(" request  projexth request\n ");
		DEBUG_LOG_INFO(DEBUG_LEVEL_2, " request  projexth request\n ");

         // 请求指定灭火器执行喷洒策略
        exth_point_group[exth_index].ExthRequestIfr(&exth_point_group[exth_index], release_policy_index, st_valve_group);
       
		// 设置指定灭火器状态为 启动中……
        exth_point_group[exth_index].IsExthStartFlag = true;
		
        // TO DO: 保存当前事件：灭火器请求启动
		EvtExthWrite(EXTH_REQUEST_OPEN_EVT);
    }
    else
    {
		DEBUG_LOG_INFO(DEBUG_LEVEL_2, "exth request func is not find \n");		
        printf("exth request func is not find \n");
    }
}



void ExthMangeCtr()
{
    uint8_t i;

    for(i = 0; i < EXTH_MAX; i++ )
    {
//		DEBUG_LOG_INFO(DEBUG_LEVEL_2, "now mange exth is %d \n", i);
        //printf("now mange exth is %d \n", i);
		
        if( exth_point_group[i].ReleasePolicGroupIfr != NULL)
        {
//			DEBUG_LOG_INFO(DEBUG_LEVEL_2, "exth policy connect \n");
            printf("exth policy connect %d \n", i);
			
            exth_point_group[i].ReleasePolicGroupIfr(&exth_point_group[i]);
        }
        else
        {
//			DEBUG_LOG_INFO(DEBUG_LEVEL_2, "exth policy is not connect %d \n", i);
            //printf("exth policy is not connect \n");
        }      
    }
}






/* 设置灭火器泵的诊断反馈*/
void ExthSetPumpDiagFeedback(ENU_EXTH_INDEX exth_index , ENU_EXTH_PUMP_INDEX Pump_index, DEV_DIAG_STATUS diagStatus)
{
	exth_point_group[exth_index].st_MainPump[Pump_index].Device_FeedBack = diagStatus;
}



/* 设置灭火器泵的诊断反馈*/
void ExthSetMainValveDiagFeedback(ENU_EXTH_INDEX exth_index , ENU_EXTH_VALVE_INDEX MainValve_index, DEV_DIAG_STATUS diagStatus)
{
	exth_point_group[exth_index].st_MainValve[MainValve_index].Device_FeedBack = diagStatus;
}



/* 获得灭火器状态 */
ENU_EXTH_STATUS GetExthStatus(ENU_EXTH_INDEX exth_index)
{
	return exth_point_group[exth_index].ExthStatus;
}
	
	
/* 获得灭火器倒计时 */
uint16_t GetExthRuntimeCountDown(ENU_EXTH_INDEX exth_index)
{
	return exth_point_group[exth_index].RunTime;
}


/* 获取指定灭火器 指定阀门的状态 */
ENU_EXTH_DEV_STATUS GetExthValveStatus(ENU_EXTH_INDEX exth_index , ENU_EXTH_VALVE_INDEX MainValve_index)
{
	return exth_point_group[exth_index].st_MainValve[MainValve_index].Device_Status;
}


/* 获取指定灭火器 指定泵的状态 */
ENU_EXTH_DEV_STATUS GetExthPumpStatus(ENU_EXTH_INDEX exth_index , ENU_EXTH_PUMP_INDEX Pump_index)
{
	return exth_point_group[exth_index].st_MainPump[Pump_index].Device_Status;
}













