/**
 * @file exth.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-09-13
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef EXTH_H
#define EXTH_H

#include "stdint.h"
#include "stdbool.h"
#include "exth_conf.h"
#include "stdio.h"





#define EXTH_CTR_PERIOD_MS	   500	// ms




// ���������ʱ���豸���״̬
typedef enum
{
	DIAG_NONE = 0X00,
	DIAG_PASS = 0X80,
	DIAG_FAIL = 0X40,
}DEV_DIAG_STATUS;




typedef enum {
    EXTH_IDEL = 0, //�������Ĭ��״̬
    EXTH_REQUEST,
    EXTH_DELAY,
    EXTH_START,
    EXTH_WAIT,
    EXTH_SUCC,
    EXTH_FAILED,
    
} ENU_EXTH_STATUS;//�����������״̬ exth status




typedef enum {
    EXTH_DEV_IDEL = 0, //���ź� �õ�Ĭ��״̬
    EXTH_DEV_REQUEST,
    EXTH_DEV_DELAY,
    EXTH_DEV_OPEN,
    EXTH_DEV_STOP,
	EXTH_DEV_FAULT,
    EXTH_DEV_FINI,
   
    
} ENU_EXTH_DEV_STATUS;//������������ŵ�״̬ exth's device status



typedef struct 
{
    DEV_BUS_POS                 Device_BusPos;      //����λ��
    ENU_EXTH_DEV_STATUS     Device_Status;      //�豸״̬
    uint8_t                 Device_FeedBack;    // �豸����: 0X80 = ͨ���� 0X40 = ʧ�ܡ�0 = δ���
}STR_EXTH_DEVICE;//�����װ���е��豸:�ܷ����ַ���ѡ��






typedef struct {
    ENU_EXTH_INDEX           Exth_Index;               // ��������������������������������ѡ���������������
    const char              *ExthName;              // ���������
    uint16_t                 Exth_Code;               // ���װ�ñ��  
   
    STR_EXTH_DEVICE             st_MainValve[EXTH_PUMP_MAX];                //����λ��
    STR_EXTH_DEVICE             st_MainPump[EXTH_VALVE_MAX];                 //����λ��
    
 
     //���������麯��ָ��

    uint8_t                 IsExthStartFlag;                      // �����������־
    ENU_EXTH_STATUS         ExthStatus;                 // ������״̬
    uint16_t                LiquidLevel;                    // Һλ
    uint16_t                PressureValue;                  // ѹ��ֵ

    uint16_t               RunTime;                    // ����ʱ��
    uint16_t               ReleaseCount;                    // ��������


    
     void (*ExthRequestIfr)(void *exth_Ptr, uint8_t release_policy_index, void *st_request_valve_group); // �����������������
    void (*ReleasePolicGroupIfr)(void *exth_Ptr);                     //���������麯��ָ��
        
} STR_EXTH_POINT;




/*�������λ*/
void ExthReset(ENU_EXTH_INDEX exth_index);

/*�������ʼ��*/
void ExthInit(const STR_CFG_EXTH *exth_CfgPtr, uint8_t exth_cfg_num);

/*���������*/
void ExthRequest(ENU_EXTH_INDEX exth_index, ENU_STRATEGY_INDEX release_policy_index, void *st_valve_group);

/*�������������*/
void ExthMangeCtr(void);


/*���� ָ������� ָ���õ� ��Ϸ���*/
void ExthSetPumpDiagFeedback(ENU_EXTH_INDEX exth_index , ENU_EXTH_PUMP_INDEX Pump_index, DEV_DIAG_STATUS status);

/*���� ָ������� ָ������ ��Ϸ���*/
void ExthSetMainValveDiagFeedback(ENU_EXTH_INDEX exth_index , ENU_EXTH_VALVE_INDEX MainValve_index, DEV_DIAG_STATUS status);

/*��ȡ��ǰ�����״̬*/
ENU_EXTH_STATUS GetExthStatus(ENU_EXTH_INDEX exth_index);

/*��ȡ��ǰ����� ����ʱʱ��*/
uint16_t GetExthRuntimeCountDown(ENU_EXTH_INDEX exth_index);

/* ��ȡָ������� ָ�����ŵ�״̬ */
ENU_EXTH_DEV_STATUS GetExthValveStatus(ENU_EXTH_INDEX exth_index , ENU_EXTH_VALVE_INDEX MainValve_index);

/* ��ȡָ������� ָ���õ�״̬ */
ENU_EXTH_DEV_STATUS GetExthPumpStatus(ENU_EXTH_INDEX exth_index , ENU_EXTH_PUMP_INDEX Pump_index);



#endif


