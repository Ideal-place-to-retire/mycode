#include "task_handle.h"

#include "RTm.h"
#include "SEGGER_RTT.h"

#include "diagnostic.h"
#include "exth.h"






struct tasks_t tasklist;
static uint8_t tasksbuf[sizeof(struct task_t)*32];



static int diag_task (struct task_args_t* args)
{
    DiagMainProcess();
    return DIAGNOSIC_PERIOD_MS; 
}



static int exthCtr_task (struct task_args_t* args)
{
	ExthMangeCtr();
    return EXTH_CTR_PERIOD; 
}



static int test_task2 (struct task_args_t* args)
{

    return 200; 
}


// 任务队列创建， 并添加任务节点
void taskInit()
{
	tasks_init_withbuf(&tasklist, tasksbuf, sizeof(tasksbuf));
	
	tasks_add_task(&tasklist,DIAGNOSIC_PERIOD_MS,diag_task,0,0,0);	
	tasks_add_task(&tasklist,EXTH_CTR_PERIOD,exthCtr_task,0,0,0);
//	tasks_add_task(&tasklist,200,test_task2,0,0,0);
	
	
}


void  doTask()
{ 
	int32_t now_ms =GetSysTime();
	tasks_schedule(&tasklist, now_ms);
 
}
