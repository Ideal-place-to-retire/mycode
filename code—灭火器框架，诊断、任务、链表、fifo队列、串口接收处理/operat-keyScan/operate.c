/**
 * 按键扫描
 * 
*/

#include "operate.h"
#include "MidDio.h"
#include "event_queue_Handle.h"
#include "exth_diage_node.h"
#include "MidAdc.h"
#include "MidDio.h"

#include "para.h"





static void Sys_Start(void);
static void Sys_Stop(void);
static void Sys_SelfCheck(void);
static void Sys_SelfLearn(void);




typedef struct 
{
	ENU_GPIO_INDEX 		gpioIndex;
	ENU_OPERAT_TYPE 	eventType;
}STR_SCAN_KEY;

STR_SCAN_KEY st_scanKeyTable[] = 
{
	{START_KEY			, OP_START_DOWN		},
	{URGENT_STOP_KEY	, OP_STOP_DOWN		},
	{SELFCHECK_KEY		, OP_SELFCHECK_DOWN },
	{REV1_KEY			, OP_REV3_DOWN		},
	{REV2_KEY			, OP_REV1_DOWN    	},
	{REV3_KEY			, OP_REV2_DOWN    	},
	{LIQUID_CHECK_KEY	, OP_LEVADAPT_DOWN  },	
};

const UINT8 keyNum = sizeof(st_scanKeyTable)/sizeof(st_scanKeyTable[0]);

/**
 * scanKey
 * void scanKey (void)
 * 无参数
 * 在定时器里调用，遍历一遍按键输入状态，有按下的按键，抛出操作事件
*/
void scanKey (void)
{
	for(int i = 0; i < keyNum; i++)
	{
		if( !DioReadChannel(st_scanKeyTable[i].gpioIndex))
		{
			EvtOpWrite(st_scanKeyTable[i].eventType);
		}
	
	}		   
}


/**
 * keyOperateHandle
 * void keyOperateHandle(void)
 * 无参数
 * 被事件master函数调用。处理操作事件。
*/

void keyOperateHandle(EVT_T  operatEvent)
{
	if(operatEvent.eEvtKind != EVT_KIND_OP)
	{
		return;
	}
	switch(operatEvent.stOpEvt) 
	{
		case OP_START_DOWN:
			
			Sys_Start();		
			break;
		
		case OP_SELFCHECK_DOWN:
			
			Sys_SelfCheck();		
			break;
		case OP_STOP_DOWN:
			
			Sys_Stop();
			break;
		
		case OP_LEVADAPT_DOWN:
			
			Sys_SelfLearn();	
			break;
		
		case OP_REV1_DOWN:
			break;
		case OP_REV2_DOWN:
			break;
		case OP_REV3_DOWN:
			break;
		
		default:
			printf("Error\n");
			break;
	}
}




/**
 * 填充系统的启动函数
*/
static void Sys_Start()
{
    printf("执行系统启动函数\n");
}


/**
 * 填充系统的关闭函数
*/
static void Sys_Stop()
{
	DEBUG_LOG_INFO(DEBUG_LEVEL_2, "SYS STOP KEY \n");

    DioWriteChannel(PIERCE_VALVE_OPEN	,  RESET);
	DioWriteChannel(BALL_VALVE_OPEN		,  RESET);
	DioWriteChannel(PIERCE_VALVE_OPEN	,  RESET);
	DioWriteChannel(BALL_VALVE_OPEN		,  RESET);
	
}                   		


/**
 * 填充系统的自检函数
*/
static void Sys_SelfCheck()
{
	DioWriteChannel(PUMP1_OPEN, 1);	
	DioWriteChannel(PUMP2_OPEN, 1);	
    DiagPumpSelfcheckStart();	
}


/**
 * 填充系统的自学习函数
*/
static void Sys_SelfLearn()
{   
	pSysPara->Full_liquid_level = AdcDmaValu(SAMPLE_LEVEL1);
	
	ParaWrite();
	
    DEBUG_LOG_INFO(DEBUG_LEVEL_2, " LEVEL LIQUID LEARN ------- \n");;
}
