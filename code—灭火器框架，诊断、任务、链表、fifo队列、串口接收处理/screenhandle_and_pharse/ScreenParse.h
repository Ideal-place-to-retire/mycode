#ifndef  _SCREENPARSE_H
#define  _SCREENPARSE_H

#include "stdtype.h"
#include "stdint.h"



#define SCREEN_CMD_BUFF_SIZE	20


typedef struct {
	void (*init)(void);
	void (*clear)(void);
    void (*append)(uint8_t c);
    void (*remove)(uint16_t n);
    uint8_t (*at)(uint16_t n);
    void (*copy)(void *des, uint16_t len);
    uint16_t (*length)(void);
    uint8_t *(*getptr)(void);
    void (*set)(uint16_t n, uint8_t c);
} STR_SCI_BUFFER;



UINT8  ScreenMakeFrame(uint8_t *buffer);

extern const STR_SCI_BUFFER * pScreenSciBuf;

#endif
