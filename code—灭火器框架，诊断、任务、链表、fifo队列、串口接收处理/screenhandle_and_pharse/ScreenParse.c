#include "ScreenParse.h"
#include "c_mem.h"
#include "ScreenHandle.h"



#define SCI_BUFFER_SIZE 2048
#define SCI_BUFFER_BOUND (SCI_BUFFER_SIZE-1)

static uint8_t gSciBuffer[SCI_BUFFER_SIZE];
static uint16_t pr, pw;

static void _SciBufferInit(void)
{
	pr = pw = 0;
	c_bzero(gSciBuffer, sizeof(gSciBuffer));
}
static void _SciBufferAppend(uint8_t c)
{
	gSciBuffer[pw] = c;
	pw = (pw + 1)&SCI_BUFFER_BOUND;
}
static uint16_t _SciBufferLength(void)
{
	return (pw - pr) & SCI_BUFFER_BOUND;
}
static void _SciBufferCopy(void *des, uint16_t len)
{
	uint16_t i;
	uint8_t *p = (uint8_t *)des;
	for (i = 0; i < len; i++) {
		*p++ = gSciBuffer[(pr + i) & SCI_BUFFER_BOUND];
	}
}

static void _SciBufferRemove(uint16_t n)
{
	uint16_t usLen;
	usLen = _SciBufferLength();
	if (n < usLen) {
		usLen = n;
	}
	pr = (pr + usLen) & SCI_BUFFER_BOUND;
}

static uint8_t _SciBufferAt(uint16_t n)
{
	return gSciBuffer[(pr + n) & SCI_BUFFER_BOUND];
}
static void _SciBufferClear()
{
	pr = pw = 0;
	c_bzero(gSciBuffer, sizeof(gSciBuffer));
}
static uint8_t * _SchBufferGetPtr(void)
{
    return gSciBuffer;
}

void _SciBufferSet(uint16_t n, uint8_t c)
{
	gSciBuffer[(pw + n)&SCI_BUFFER_BOUND] = c;
}


const STR_SCI_BUFFER stSciBuffer = {
	_SciBufferInit,
	_SciBufferClear,
	_SciBufferAppend,
	_SciBufferRemove,
	_SciBufferAt,
	_SciBufferCopy,
	_SciBufferLength,
	_SchBufferGetPtr,
};
const STR_SCI_BUFFER * pScreenSciBuf = &stSciBuffer;




#ifndef	SCREEN_MESSAGE

#define SCREEN_HEAD1 0X5A		// 报文帧头1
#define SCREEN_HEAD2 0XA5  	// 报文帧头2

#define SCREEN_TAIL1 0X9A		// 报文帧尾1
#define SCREEN_TAIL2 0X9B      // 报文帧尾2

#endif

#define SCREEN_FUNC_OFFSET_ADD		3


UINT8  ScreenMakeFrame(uint8_t *buffer)
{
	c_bzero(buffer, SCREEN_CMD_BUFF_SIZE);

    while(_SciBufferLength() != 0)
    {
        if(_SciBufferAt(0) == SCREEN_HEAD1)
        {
            if(_SciBufferAt(1) == SCREEN_HEAD2 && _SciBufferAt(_SciBufferAt(2)+1) == SCREEN_TAIL1 && _SciBufferAt(_SciBufferAt(2)+2) == SCREEN_TAIL2)
            {
				for(int i = 0; i < _SciBufferAt(2) - 2; i++)
				{
                	buffer[i] = _SciBufferAt(i + SCREEN_FUNC_OFFSET_ADD);
					
				}
                _SciBufferRemove(_SciBufferAt(2)+4);
               return 1;
            }
            else
            {
               _SciBufferRemove(1);
            }
        }
        else
        {
            _SciBufferRemove(1);
        }

    }
	return 0;
}





