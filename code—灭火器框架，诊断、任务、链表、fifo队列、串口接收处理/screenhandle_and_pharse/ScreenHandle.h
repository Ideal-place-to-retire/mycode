/*! 
*  \file ScreenHandle.h
*  
*/

#ifndef _SCREENHANDLE_H
#define _SCREENHANDLE_H


#include "stdint.h"




#define	SCREEN_MESSAGE

#define SCREEN_HEAD1 0X5A		// 报文帧头1
#define SCREEN_HEAD2 0XA5  	// 报文帧头2

#define SCREEN_TAIL1 0X9A		// 报文帧尾1
#define SCREEN_TAIL2 0X9B      // 报文帧尾2


/*FUNC CODE*/

#define CMD_REQUEST_0X90 		 	0x90
#define CMD_ASK_VERSION_0X91		0X91
#define CMD_ASK_FAULT_0X92			0X92
#define CMD_ASK_OPER_0X93			0X93
#define CMD_ASK_EXTH_0X94			0X94

#define CMD_EXTH_CONF_0X95			0X95
#define CMD_CLEAR_MESSG_0X96		0X96
#define CMD_CHECK_TIME_0X97			0X97
#define CMD_ID_CONF_0X98			0X98
#define CMD_DEV_RESET_0X99			0X99



void ScreenCmdHandle(uint8_t *buf);









///TEST

//void ScreenReset(uint8_t *buf);
//void ScreenIdConfig(uint8_t *buf);
//void ScreenCheckTime(uint8_t *buf);
//void ScreenFaultRcv(void);

void ScreenDataRcv(void);

#endif
