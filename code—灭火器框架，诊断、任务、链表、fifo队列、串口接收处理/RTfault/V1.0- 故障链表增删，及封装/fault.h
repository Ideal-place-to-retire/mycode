#ifndef _FAULT_H
#define _FAULT_H

#include "tasks.h"
//#include "history_record.h"
//#include "screen_mainHandle.h"

#include "stdint.h"
#include "nodebuf.h"
#include "evtqueue.h"



typedef struct 
{
    EVT_T data;
    struct nodebuf_t faultbuf;
    struct list_head flist;

} Node;



void test_dofault(void);

void RtFaultInit(void);
void faultHandle (EVT_T   faultEvent);
void RtFaultWrite(EVT_T  faultEvent);
void RtFaultRead(UINT16 serialNum, EVT_T *evt);
void RtFaultDelet(EVT_T faultEvent);
UINT16 RtFaultNum(void);

#endif


