#ifndef _FAULT_H
#define _FAULT_H

#include "mem_malloc.h"
#include "tasks.h"
//#include "history_record.h"
//#include "screen_mainHandle.h"

#include "stdint.h"
#include "nodebuf.h"
#include "evtqueue.h"



typedef struct 
{
    EVT_T data;
    struct nodebuf_t faultbuf;
    struct list_head flist;

} Node;



void test_dofault();

void fault_init_withbuf(Node *faults, void *buf, int buflen);
void faultHandle (EVT_T   faultEvent);
void fault_write(Node *faults, EVT_T  faultEvent);
EVT_T fault_read(Node *faults, UINT8 num);


#endif