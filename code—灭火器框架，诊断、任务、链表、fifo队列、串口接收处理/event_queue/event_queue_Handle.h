#ifndef EVTQUEUEINIT_H
#define EVTQUEUEINIT_H

#include "evtqueue.h"
#include "stdtype.h"


void eventQueueInit(void);
void doEvtqueue(void);


INT32 EvtOpWrite(ENU_OPERAT_TYPE eOpType);
INT32 EvtFaultWrite(ENU_FAULT_TYPE eFaultType) ;
INT32 EvtExthWrite(ENU_EXTH_TYPE eExthType);

#endif


