#include "event_queue_Handle.h"


#include "fault.h"
#include "RTm.h"
#include "operate.h"



static void handleEvent(EVT_T evt);

void eventQueueInit()
{
	evtqueue_init();
}





void doEvtqueue()
{
    EVT_T evts[32];	
	int n = evtqueue_read(evts, 32);    
	
	for (int i = 0; i < n; i++) 
	{   
		handleEvent(evts[i]);	
	}
  
}





static void handleEvent(EVT_T evt)
{
	switch(evt.eEvtKind)
	{
		case EVT_KIND_OP:
			
			keyOperateHandle(evt);
		
		    //DEBUG_LOG_INFO(DEBUG_LEVEL_2, "EVT_KIND_OP \n");
		
			break;
		
		case EVT_KIND_FAULT:
			
			faultHandle (evt);
			
			//DEBUG_LOG_INFO(DEBUG_LEVEL_2, "EVT_KIND_FAULT \n");
			
			break;
		
		case EVT_KIND_EXTH:
			
		
			//DEBUG_LOG_INFO(DEBUG_LEVEL_2, "EVT_KIND_EXTH \n");

			break;
		
		case EVT_EXIT:
			break;
	}

}






/**
 * @brief 写操作事件
 * @param eOptype 操作类型
 * @return int32_t 返回写入个数，返回0表示已满。
 */
INT32 EvtOpWrite(ENU_OPERAT_TYPE eOpType) 
{
    EVT_T evt = {
        .eEvtKind = EVT_KIND_OP,
        .stOpEvt = eOpType,
        .time = GetRTime(),
    };

    return evtqueue_write(&evt,1);
}



/**
 * @brief 写故障事件
 * @param eFaultType 故障事件类型
 * @return int32_t 返回写入个数，返回0表示已满。
 */
INT32 EvtFaultWrite(ENU_FAULT_TYPE eFaultType) 
{
    EVT_T evt = {
        .eEvtKind = EVT_KIND_FAULT,
        .stFaultEvt = eFaultType,
        .time = GetRTime(),
    };
    return evtqueue_write(&evt,1);

}

/**
 * @brief 写灭火器启动事件
 * @param eOptype 灭火器启动类型
 * @return int32_t 返回写入个数，返回0表示已满。
 */
INT32 EvtExthWrite(ENU_EXTH_TYPE eExthType) 
{
    EVT_T evt = {
        .eEvtKind = EVT_KIND_EXTH,
        .stExthEvt = eExthType,
        .time = GetRTime(),
    };

    return evtqueue_write(&evt,1);
}












