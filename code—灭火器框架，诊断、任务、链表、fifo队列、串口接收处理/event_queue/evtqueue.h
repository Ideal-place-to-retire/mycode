/**
* @file  evtqueue.h  
* @brief 事件队列
* @author ls
* @date  2023-6-14 
* @version   0.1
* @copyright  创为新能源科技股份有限公司                                                      
*/

#ifndef _EVT_QUEUE_H_
#define _EVT_QUEUE_H_

#include "stdtype.h"
#include "kfifo_sp.h"
#include "SEGGER_RTT.h"


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 事件类型
 */
typedef enum {
    EVT_EXIT  = 0,
    EVT_KIND_OP,       // 操作事件
    EVT_KIND_FAULT,    // 故障事件
    EVT_KIND_EXTH     // 灭火器启动事件
} ENU_EVT_KIND;

/**
 * @brief 操作事件
 */
typedef enum 
{
    OP_START_DOWN = 0,       // 启动键按下
    OP_SELFCHECK_DOWN,       // 自检按键按下
    OP_STOP_DOWN,            // 停止按键按下
    OP_LEVADAPT_DOWN,        // 自适应按键
    OP_REV1_DOWN,            // 预留按键1
    OP_REV2_DOWN,            // 预留按键2
    OP_REV3_DOWN,            // 预留按键3

//    OP_SWITCH_ADMIN,         // 切换管理员权限
//    OP_SWITCH_NORMAL,        // 切换普通权限

} ENU_OPERAT_TYPE;

/**
 * @brief 故障事件
*/
typedef enum 
{
    FAULT_PUMP_SELFCHECK = 0,  // 泵自检故障
    FAULT_PIERCEVAL_ABNORM,    // 刺破阀异常
    FAULT_BALLVAL_ABNORM,      // 球阀异常  
    FAULT_BMS,                 // BMS故障
    FAULT_BAT_LOW,             // 电池电量低
    FAULT_EXTH_STARTED,        // 灭火器已启动
    TAULT_LEVEL_LOW,           // 液位低
    FAULT_24V_L,               // 24V过压
    FAULT_24V_H,               // 24V欠压
    FAULT_5V_H,                // 5V过压
    FAULT_5V_L,                // 5V欠压


    
} ENU_FAULT_TYPE;

/**
 * @brief 灭火器启动事件
 * 
 */
typedef enum 
{
    EXTH_REQUEST_OPEN_EVT = 0,          // 灭火器请求启动
    EXTH_MAINVAL_OPEN_EVT ,             // 启动刺破阀
    EXTH_BALLVAL_OPEN_EVT ,             // 启动球阀
    EXTH_PUMP_OPEN_EVT ,                // 启动泵
    EXTH_MAINVAL_FAULT_CLOSE_EVT ,      // 刺破阀故障关闭
    EXTH_BALLVAL_FAULT_CLOSE_EVT ,      // 球阀故障关闭
    EXTH_RELEASE_START_EVT,             // 喷洒开始
    EXTH_PUMP_FAULT_CLOSE_EVT,          // 泵故障关闭
    EXTH_RELEASE_PAUSE_EVT,             // 喷洒暂停
    EXTH_RELEASE_OVER_EVT,              // 喷洒结束                                                                                               

} ENU_EXTH_TYPE; 

/**
 * @brief 事件
 */
typedef struct 
{
    ENU_EVT_KIND eEvtKind;
    union
    {
        ENU_OPERAT_TYPE     stOpEvt;
        ENU_FAULT_TYPE      stFaultEvt;
        ENU_EXTH_TYPE       stExthEvt;
    };
    UINT32    time;
} EVT_T;

void    evtqueue_init(void);
INT32 evtqueue_write(const EVT_T *evt, INT32 force);
INT32 evtqueue_read(EVT_T *evt, UINT32 n);




#ifdef __cplusplus
} // extern "C"
#endif

#endif // EVT_QUEUE_H_
