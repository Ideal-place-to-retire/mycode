/**
 * @file  evtqueue.c
 * @brief 事件队列
 * @author ls
 * @date  2023-6-14
 * @version   0.1
 * @copyright  创为新能源科技股份有限公司
 */

#include "evtqueue.h"



DECLARE_KFIFO(evtq_buf, EVT_T, 1024); //定义事件队列

/**
 * @brief 初始化
 */
void evtqueue_init() 
{      
    INIT_KFIFO(evtq_buf);
}

/**
 * @brief 向消息队列尾部写入一个消息。
 * @param evt 消息
 * @param force 如果参数force非零，在队列已满的情况下自动丢弃队列头部的一个消息，以确保送出此条消息。
 * @return int32_t 返回0表示队列已满，写入事件个数
 * @attention 内部复制参数evt，故调用后不再使用该指针。
 */
INT32 evtqueue_write(const EVT_T *evt, INT32 force)
{
    INT32 n = kfifo_in(&evtq_buf, evt, 1);
	
    if (n == 0) 
	{
        if (force) 
		{
            static EVT_T dummy ;
            kfifo_out(&evtq_buf, &dummy, 1);
            n = kfifo_in(&evtq_buf, evt, 1);
        } 
		else 
		{
            
        }
    }
    return n;
}

/**
 * @brief 从消息队列读消息
 * @param evt 事件
 * @param n 从消息队列头部取出n个消息
 * @return int32_t 大于0表示取出的消息个数，返回0表示队列已空。
 */
INT32 evtqueue_read(EVT_T *evt, UINT32 n) 
{
    INT32 r = kfifo_out(&evtq_buf, evt, n);
    return r;
}




