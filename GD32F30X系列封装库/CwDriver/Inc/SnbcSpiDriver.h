/**
* Copyright (c) 2017,山东新北洋信息技术股份有限公司
* All rights reserved.
*
* @file			SnbcSpiDriver.h
* @brief		SPI驱动头文件
* @author		peibibao
* @date			2018-01-15
* @version		V1.0
* @attention
*/

#ifndef _SNBCSPIDRIVER_H
#define _SNBCSPIDRIVER_H

#include "StdSnbc.h"

//////////////////////////////////
///@defgroup SPIDriver
///@{
//////////////////////////////////

//#define _SPI_CS_BY_GPIO_DEF				/*定义该宏则SPI的CS片选使用GPIO控制*/
//#define _SPI_DMA_TRANSFER_FIFO_DEF	    /*定义该宏则启用DMA传输FIFO*/

/**
* @brief  SPI速率
*/
typedef enum
{
	SPI_BANDRATE_42M    = 0,
	SPI_BANDRATE_21M    = 1,
	SPI_BANDRATE_10_5M  = 2,
	SPI_BANDRATE_5_25M  = 3,
	SPI_BANDRATE_2_625M = 4,
	SPI_BANDRATE_1_3125M,
	SPI_BANDRATE_0_65625M,
}BANDRATE;

/**
 * @brief 中断分类
 */
typedef enum
{
	DRV_SPI_INT_TX				= 1,		///< 发送中断		= BIT0
	DRV_SPI_INT_RX				= 2,		///< 接收中断		= BIT1
	DRV_SPI_INT_TRANS_COMPLETE	= 4,		///< 传输完成中断	= BIT2
	DRV_SPI_INT_RX_OVERFLOW		= 8,		///< 接收溢出		= BIT3
}DRV_SPI_INT_TYPE;

/**
* @brief SPI ID
*/
typedef enum
{
    DRV_SPI_ID_0 = 0,
    DRV_SPI_ID_1 = 1,
    DRV_SPI_ID_2 = 2,
    DRV_SPI_ID_3 = 3,
    DRV_SPI_ID_4 = 4,
    DRV_SPI_ID_5 = 5,
    DRV_SPI_ID_6 = 6,
    DRV_SPI_ID_7 = 7,
    DRV_SPI_ID_8 = 8,
    DRV_SPI_ID_9 = 9,
}DRV_SPI_ID;

/**
* @brief SPI CS
*/
typedef enum
{
    DRV_SPI_CS_0 = 0,
    DRV_SPI_CS_1 = 1,
    DRV_SPI_CS_2 = 2,
    DRV_SPI_CS_3 = 3,
    DRV_SPI_CS_4 = 4,
	DRV_SPI_CS_5 = 5,
    DRV_SPI_CS_6 = 6,
    DRV_SPI_CS_7 = 7,
}DRV_SPI_CS;

/**
* @brief SPI HOLD
*/
typedef enum
{
    DRV_SPI_HOLD_NONE   = 0,		///< 空闲时 SPI CS的状态
    DRV_SPI_HOLD_ACTIVE = 1,		///< 发送前拉低CS
    DRV_SPI_HOLD_CLR    = 2,		///< 发送后拉高CS
	DRV_SPI_HOLD_ACTIVE_AND_CLR    = 3,		///< 发送前拉低CS 并且 发送后拉高CS
}DRV_SPI_HOLD;

/**
* @brief SPI操作模式
*/
typedef enum
{
    DRV_SPI_MASTER = 0,				///< SPI 为主模式工作
    DRV_SPI_SLAVE  = 1,				///< SPI 为从模式的方式工作
}DRV_SPI_MODE;


/**
* @brief SPI时钟模式
*/
typedef enum
{
    DRV_SPI_CLK_PH_0_POL_0 = 0,		///< 空闲时时钟为低电平  并且在第一个沿采样
    DRV_SPI_CLK_PH_0_POL_1 = 1,		///< 空闲时时钟为低电平  并且在第二个沿采样
    DRV_SPI_CLK_PH_1_POL_0 = 2,		///< 空闲时时钟为高电平  并且在第一个沿采样
    DRV_SPI_CLK_PH_1_POL_1 = 3,		///< 空闲时时钟为高电平  并且在第二个沿采样
}DRV_SPI_CLK_MODE;

/**
* @brief SPI片选模式，手动或者自动
*/
typedef enum
{
    DRV_SPI_CS_NOMARL = 0,			///< 片选需要用户控制
    DRV_SPI_CS_AUTO   = 1,			///< 片选不需要用户控制
}DRV_SPI_CS_MODE;

/**
* @brief SPI数据移动方向
*/
typedef enum
{
    DRV_SPI_DATA_MSB = 0,			///<  高位在前
    DRV_SPI_DATA_LSB = 1,			///<  低位在前
}DRV_SPI_DATA_SHIFT_DIR;

typedef struct
{
	U8 mDelayNss;					///< 传输字间隔时片选输出无效的延时，用于自动片选模式
	U8 mDelayBtwn;					///< 两个片选的延时，用于从模式
	U8 mDelayAfter;					///< 两个传输字节之间的延时
	U8 mDelayInit;					///< 从片选设置至第一个字节开始传输的延时，用于自动片选模式
}DRV_SPI_DELAY;

/**
* @brief 配置结构体
*/
typedef struct
{
	U32						mClkFreq;		///<SPI 运行的波特率
	DRV_SPI_CLK_MODE		mClkMode;		///<SPI 数据采样模式
	DRV_SPI_DATA_SHIFT_DIR	mDataShiftDir;	///<SPI数据传输方向
	DRV_SPI_CS_MODE			mCsMode;		///<片选模式
	DRV_SPI_DELAY			mDelay;			///<片选的延时时间，在STM32中没用
}DRV_SPI_CONFIG;

/**
* @brief	SPI设备初始化
*
* @param	SpiId			SPI编号
* @param	SpiMode			SPI工作模式，主或从
* @param	CsPinSelect		SPI片选引脚
* @param	Config			SPI配置
*
* @note		CS可以传0下来，表示不用底层操作片选
* @return	0成功，其他失败
*/
extern S32 DrvSpiDeviceInit(const DRV_SPI_ID SpiId, const DRV_SPI_MODE SpiMode, const U8 CsPinSelect, DRV_SPI_CONFIG *const Config);

/**
* @brief	SPI配置设置
*
* @param	SpiId	SPI编号
* @param	Config	SPI配置指针，包括:	mClkFreq频率的设置
*										mClkMode模式的设置
*										mDataShiftDir高低位设置
* @return	成功返回0，失败返回非0
*/
extern S32 DrvSpiConfigSet(const DRV_SPI_ID SpiId, DRV_SPI_CONFIG *const Config);

/**
* @brief    SPI阻塞传输
*
* @param    SpiId         SPI编号
* @param    SpiCs         SPI片选选择
* @param    CsHoldEn      SPI片选控制使能
* @param    SendBuf       发送缓冲区，只接收时可为NULL
* @param    RecvBuf       接收缓冲区，只发送时可为NULL
* @param    Length        传输长度
* @return	成功返回0，失败返回非0
 */
extern S32 DrvSpiBlockTransfer(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, const DRV_SPI_HOLD CsHoldEn, U8 *const SendBuf, U8 *const RecvBuf, U32 Length);

/**
* @brief    SPI中断传输
*
* @param    SpiId         SPI编号
* @param    SpiCs      	  SPI片选选择
* @param    CsHoldEn      SPI片选控制使能
* @param    SendBuf       发送缓冲区，只接收时可为NULL
* @param    RecvBuf       接收缓冲区，只发送时可为NULL
* @param    Length        传输长度
* @return	成功返回0，失败返回非0
 */
extern S32 DrvSpiIntTransfer(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, const DRV_SPI_HOLD CsHoldEn, U8 *const SendBuf, U8 *const RecvBuf, U32 Length);


/**
* @brief	Spi DMA模式数据传输
* @param	SpiId		SPI编号
* @param	SpiCs		SPI的片选
* @param	CsHoldEn	SPI的片选保持是否使能
* @param	SendBuf		发送缓冲
* @param	RecvBuf		接受缓冲
* @param	Length		传输数据的长度
* @return	0成功，其他失败
*/
extern S32 DrvSpiDmaTransfer(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, const DRV_SPI_HOLD CsHoldEn, U8 *const SendBuf, U8 *const RecvBuf, U32 Length);

/*
* @brief    设置DMA传输完成后中断回调函数
*
* @param    SpiId         SPI编号
* @param    DmaCompleteCallBack       传输完成中断回调函数指针
* @return	0成功，其他失败
*/
extern S32 DrvSpiDmaCompleteIntCallBackSet(const DRV_SPI_ID SpiId, void *DmaCompleteCallBack);

/**
* @brief	Spi中断回调函数注册
* @param	SpiId		SPI编号
* @param	IntCallBack	中断回调函数
* @return	成功返回0，失败返回非0
*/
extern S32 DrvSpiIntCallBackSet(const DRV_SPI_ID SpiId, void (*IntCallBack)(U32 TransLen, DRV_SPI_INT_TYPE IntType));

/*
* @brief    获取DMA已传输长度
*
* @param    SpiId         SPI编号
*
* @return   DMA传输长度
*/
extern U32 DrvGetSpiDmaTransLen(const DRV_SPI_ID SpiId);

/**
* @brief	Spi中断使能设置
* @param	SpiId		SPI编号
* @param	Mask		中断掩码 @ref SPI_INT_TYPE
* @param	Enable		使能/禁止，使用宏ENABLE,DISABLE
* @return	0成功，其他失败
*/
extern S32 DrvSpiIntEnSet(const DRV_SPI_ID SpiId, const U8 Mask, const U8 Enable);

/**
* @brief	Spi DMA中断使能
* @param	SpiId		SPI编号
* @param	Enable		使能/禁止，使用宏ENABLE,DISABLE
* @return	0成功，其他失败
*/
extern S32 DrvSpiDmaIntEnSet(const DRV_SPI_ID SpiId, const U8 Enable);

/**
* @brief	片选操作
*
* @param	SpiId		SPI编号
* @param	SpiCs		选择从设备 ，有多个从设备时 ，通过该参数选择
* @param	CsHoldEn	0:拉低，非0:拉高
* @return	0成功，其他失败
*/

extern U8 DrvSpiCsSet(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, int CsHoldEn);

/**
* @brief	SPI Qmode使能设置
*
* @param	SpiId		SPI编号
* @param	Enable		使能或禁止, STD_ENABLE or STD_DISABLE
* @return	0成功，其他失败
*/
extern S32 DrvSpiQModeEnSet(const DRV_SPI_ID SpiId,  U32 Enable);

/**
* @brief	SPI Qmode模式下读写设置
*
* @param	SpiId		SPI编号
* @param	RW		    1-使能写， 0-使能读
* @return	0成功，其他失败
*/
extern S32 DrvSpiQModeRwSet(const DRV_SPI_ID SpiId,  U32 RW);

//////////////////////////////////
///@}
//////////////////////////////////

#endif
