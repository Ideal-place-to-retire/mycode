/********************************************************************
版权(C),烟台创为新能源科技有限公司
文件名：DrvAdc.h
作者: JH
版本号:1.0
生成日期:2023.4.11
概述: ADC驱动
修改日志：
*********************************************************************/
#ifndef _DRVADC_H
#define _DRVADC_H
#include "DataTypeDef.h"
#include "gd32f30x_libopt.h"

typedef enum
{
    ADC_0 = 0,
    ADC_1,
    ADC_2,
    ADC_PERIPH_NUM,
}ADC_PERIPH;

typedef struct
{
    U8 Index;
    U8 Channel;
}CHANNEL_CONFIG;

typedef struct
{
    U32 ADCx;
    U32 CLKx;
    U32 Resolution;
    U32 DMAx;
    rcu_periph_enum  RCUx;
    rcu_periph_enum  DMA_RCU;
    dma_channel_enum DMA_CHx;
}ADC_RESOURCE;

extern void DrvAdcGpioInit(U32 GpioPort, U32 GpioPin);
extern void DrvAdcRcuConfig(ADC_PERIPH Periph);
extern void DrvADCNormalInit(ADC_PERIPH Periph);
extern S8 DrvADCUseDmaInit(ADC_PERIPH Periph, U8 DMAChNum, const CHANNEL_CONFIG* ConfigPtr);
extern F32 DrvAdcChannelSample(ADC_PERIPH Periph, U8 Channel);
extern F32 DrvGetDmaChannelAdValue(U8 Channel);

#endif

