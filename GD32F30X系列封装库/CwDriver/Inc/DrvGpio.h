/********************************************************************
版权(C),烟台创为新能源科技有限公司
文件名：DrvGpio.h
作者: JH
版本号:1.0
生成日期:2023.4.4
概述: Gpio驱动
修改日志：
*********************************************************************/
#ifndef _DRVGPIO_H
#define _DRVGPIO_H
#include "DataTypeDef.h"
#include "gd32f30x.h"

typedef void (*GpioIntCallBack)(void);

extern void DrvSWDInit(void);
extern void DrvGpioInit(U32 Port, U32 Pin, U8 GpioMode);
extern void DrvGpioOperat(U32 Port, U32 Pin, BOOL Set);
extern void DrvGpioExitSetSource(U8 SrcPort, U8 SrcPin, exti_trig_type_enum Type);
extern void DrvSetExitCallBack(U8 SrcPin, GpioIntCallBack CallBackFunc);
extern BOOL DrvGpioGetInputBit(U32 Port, U32 Pin);
extern BOOL DrvGpioGetOutputBit(U32 Port, U32 Pin);

#endif

