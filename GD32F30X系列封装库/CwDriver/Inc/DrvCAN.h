/********************************************************************
版权(C),烟台创为新能源科技有限公司
文件名：DrvCAN.h
作者: JH
版本号:1.0
生成日期:2023.4.4
概述: CAN驱动
修改日志：
*********************************************************************/

#ifndef _DRVCAN_H
#define _DRVCAN_H
#include "DataTypeDef.h"
#include "gd32f30x_libopt.h"
#include "gd32f30x_rcu.h"

typedef enum
{
    CAN_0 = 0,
    CAN_1,
    CAN_PERIPH_NUM,
}CAN_PERIPH;

typedef struct 
{
    U32 mCANx;
    U32 mRxPort;
    U32 mRxPin;
    U32 mTxPort;
    U32 mTxPin;
    U32 mRemap;
    U8  mCanMode; 
    rcu_periph_enum mCAN_RCU;
}CAN_RESOURCE;

/**
 * @brief 错误分类
 */
typedef enum
{
    DRV_CAN_ERR_BIT         = BIT(0),			///< 位错误
    DRV_CAN_ERR_STUFF       = BIT(1),			///< 填充错误
    DRV_CAN_ERR_CRC         = BIT(2),			///< CRC错误
    DRV_CAN_ERR_FORM        = BIT(3),			///< 格式错误
    DRV_CAN_ERR_ACK         = BIT(4),			///< Ack错误
}DRV_CAN_ERR_TYPE;

extern void (*CAN0TxInterruptCallBack)(void);
extern void (*CAN0RxInTerruptCallBack)(U8* DataPtr, U8 DataLen);

extern void (*CAN1TxInterruptCallBack)(void);
extern void (*CAN1RxInTerruptCallBack)(U8* DataPtr, U8 DataLen);

extern S32 DrvCanDeviceInit(CAN_PERIPH CanPeriph, U32 BaudRate);
extern S32 DrvCanFilterConfig(CAN_PERIPH CanPeriph, U32 Mask, U32 CanId, U8 FilterNum);
extern void DrvCanInterruptSet(CAN_PERIPH CanPeriph, U32 ItType, BOOL Enable);
extern U8  DrvCanSendData(CAN_PERIPH CanPeriph,U32 FrameFormat, U32 SendID, U8 *const SendData, U16 DataLength);
extern U32 DrvGetCanError(CAN_PERIPH CanPeriph);

#endif 
