/********************************************************************
版权(C),烟台创为新能源科技有限公司
文件名：DrvUsart.h
作者: JH
版本号:1.0
生成日期:2023.4.13
概述: Usart驱动
修改日志：
*********************************************************************/
#ifndef _DRVUSART_H
#define _DRVUSART_H
#include "DataTypeDef.h"
#include "gd32f30x.h"

#if defined(__CC_ARM) 
#pragma diag_suppress 1296
#elif defined(__ICCARM__)
#elif defined(__GNUC__)
#endif


typedef void (*UsartIntCallBack)(U8* Data, U8 Size);

typedef enum
{
    USART_0 = 0,
    USART_1,
    USART_2,
	UART_3,
    USART_PERIPH_NUM,
}USART_PERIPH;

/**
* @brief 串口DMA配置
*/
typedef struct
{
    U32					mPeriph;				///< DMA基地址
	dma_channel_enum	mChannel;			    ///< DMA通道编号
	IRQn_Type		  	mIrq;       			///< 中断编号
	U32					mIrqFlag;				///< 中断标志位
    U32                 mUsartAddr;             ///< 外设地址
    U32                 mDmaMemory;             ///< 内存地址
    U8                  mMemorySize;            ///< 接收数据大小
	U8					mDmaInx;				///< DMA编号
    
}USART_DMA_RESOURCES;

/**
* @brief 串口配置
*/
typedef struct
{
    U32					mPeriph;				///< 设备地址
    U32					mSendPort;				///< 发送端口
    U32					mSendPin;				///< 发送引脚
    U32					mRecPort;		        ///< 接收端口
    U32					mRecPin;				///< 接收引脚
    U32                 mRemap;                 ///< 重映射
    rcu_periph_enum     mUsartRcu;              ///< 串口时钟
	
}USART_RESOURCES;

/**
 * @brief 配置结构体
 */
typedef struct
{
    U32             mBaudRate;				///< 波特率
    U8              mParityMode;			///< 校验方式
    U8              mDataBit;				///< 数据位数
    U8              mStopBit;				///< 停止位数
}DRV_USART_CONFIG;

/**
* @brief 中断配置
*/
typedef enum
{
	USART0_TBE = 0,
    USART0_TC,
    USART0_RBNE,
    USART0_IDLE,
    USART1_TBE,
    USART1_TC,
    USART1_RBNE,
    USART1_IDLE,
    USART2_TBE,
    USART2_TC,
    USART2_RBNE,
	USART2_IDLE,   
	UART3_TBE,
    UART3_TC,
    UART3_RBNE,
    UART3_IDLE,
    USART_INT_SET_NUM,
}USART_INT_SET;

/**
* @brief 中断配置
*/
typedef struct
{
	IRQn_Type                 IrqType; 
    usart_interrupt_enum      Irq;
    usart_interrupt_flag_enum IrqFlag;
    U8                        nvic_irq_pre;
    U8                        nvic_irq_sub;
}USART_INT_MANAGE;

extern void DrvUsartGpioConfig(USART_PERIPH Periph);
extern void DrvUsartInit(USART_PERIPH Periph, DRV_USART_CONFIG* Config);
extern void DrvUsartInterruptConfig(USART_PERIPH Periph, USART_INT_MANAGE* IrqManage, BOOL Enable);
extern void DrvUsartInterruptCallBack(USART_INT_SET IrqSet, UsartIntCallBack CallBackPtr);
extern void DrvUsartRxDmaConfig(USART_PERIPH Periph,const U8* RxDataBuf,const U8 RxNum);
extern void DrvUsartTxDmaConfig(USART_PERIPH Periph);
extern S8 DrvUsartBlockSendData(USART_PERIPH Periph, CHAR *const SendBuf, U8 TxSize);
extern S8 DrvUsartDmaSendData(USART_PERIPH Periph, U8 *const SendBuf, U8 TxSize);
extern S8 DrvUsartBlockRecData(USART_PERIPH Periph,U8 *RecvBuf,U8 RxSize);

#endif
