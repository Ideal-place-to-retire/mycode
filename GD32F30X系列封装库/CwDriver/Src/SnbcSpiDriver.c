/**
* Copyright (c) 2017,山东新北洋信息技术股份有限公司
* All rights reserved.
* 
* @file			SnbcSpiDriver.c
* @brief		SPI驱动
* @author		wangbotao
* @version		V1.0
* @date			2017-09-23
* @attention	SPI的实际输出频率和设置频率存在偏差，对应关系如下：
                设置频率                     实际频率
				SPI_BANDRATE_42M   			 30M 		
				SPI_BANDRATE_21M    		 15M
				SPI_BANDRATE_10_5M  		 7.5M
				SPI_BANDRATE_5_25M 			 3.75M
				SPI_BANDRATE_2_625M 		 1.875M
				SPI_BANDRATE_1_3125M,		 0.9375M
				SPI_BANDRATE_0_65625M,		 0.46875M	
				
				SPI0 时钟源频率为120,能频到的频率为
				30M，15M，7.5M，3.75M，1.875M，0.9375M，0.46875M	
				SPI1 SPI2 时钟源频率为60,能频到的频率为
				30M，15M，7.5M，3.75M，1.875M，0.9375M，0.46875M，0.234375M					
*/
#include "gd32f30x.h"
#include "gd32f30x_rcu.h"
#include "gd32f30x_misc.h"
#include "gd32f30x_dma.h"
#include "gd32f30x_spi.h"
#include "StdSnbc.h"
#include "SnbcDriverConfig.h"
#include "SnbcPinMux.h"
#include "SnbcSpiDriver.h"


#define SPI_NUM_MAX		        (3U)		   	///< 模块内SPI最大计数
#define SPI_BANDRATE_ENUM_MAX   10              ///< 枚举值的最大值，超过该值后均按真实值的波特率配置
#define SPI_BANDRATE_MAX_DIV    7               ///<  spi的最大分频为7分频
/**
*@brief SPI收发数据缓冲结构
*/
typedef struct
{
    U32	mCurIndex;	                            ///< 在进行中断时接收或发送数据时的当前数据索引
    U8	*mSendBuf;	                            ///< 指向发送的数据缓冲区
    U8	*mRcvBuf;	                            ///< 指向接收的数据缓冲区
    U32	mLen;		                            ///< 要接收或发送的数据长度
}SNBC_DEV_SPI;

static SNBC_DEV_SPI sSnbc_Dev_Spi[SPI_NUM_MAX];	///< 定义SPI的数据结构 用于存放数据的发送与接收
static U32 sSpiDma_RcvLen[SPI_NUM_MAX];			///< 定义记录发送或接收的数据长度，用于计算还有多少字节需要发送或接收
static S32 sSpiWorkMode[SPI_NUM_MAX] = {0};    	///< 0 : 单工模式; -1: 双工模式; -2: 4线模式

/**
* @brief SPI外设基地址
*/
const static U32 sSpiBase[]=
{
	SPI0, SPI1, SPI2
};

/**
* @brief SPI DMA配置
*/
typedef struct
{
	dma_channel_enum	mCh;		///< DMA通道编号
	U32					mPeriph;	///< DMA基地址
	IRQn_Type		    mIrq;       ///< 中断编号
	U32					mIrqFlag;	///< 中断标志位
	U8					mDmaInx;	///< DMA编号
}SPI_DMA_RESOURCES;

/**
* @brief SPI的DMA配置表
*/
static const SPI_DMA_RESOURCES sSpiDmaResource[][2] =
{
	{
		SPI0_TX_DMA_RESOURCES,
		SPI0_RX_DMA_RESOURCES,
	},
	{
		SPI1_TX_DMA_RESOURCES,
		SPI1_RX_DMA_RESOURCES,
	},
	{
		SPI2_TX_DMA_RESOURCES,
		SPI2_RX_DMA_RESOURCES,
	} 
};

static U32 DrvGetSpiBusClock(const DRV_SPI_ID SpiId)
{
    U32 BusClock;
    
    if(DRV_SPI_ID_0 == SpiId)
    {
        BusClock = rcu_clock_freq_get(CK_APB2);
    }
    else
    {
        BusClock = rcu_clock_freq_get(CK_APB1);
    }

    return BusClock;
}
/**
* @brief SPI的中断回调函数表
*/
static void (*Spi_sSpiIntCallBack[])(U32 TransLen,DRV_SPI_INT_TYPE IntType) =
{
    NULL, NULL, NULL
};

extern S32 Dma_SetCallBack(U8 dma,U8 ch,void *pfunc);

/**
* @brief	获取SPI的默认配置参数
* @param	*Config		返回SPI的默认配置参数
* @return	无
*/
void DrvSpiGetDefaultConfig(DRV_SPI_CONFIG *Config)
{
    if( NULL == Config)
    {
        return;
    }
    
    Config->mClkFreq		= SPI_BANDRATE_2_625M;				///< 时钟频率
    Config->mClkMode		= DRV_SPI_CLK_PH_0_POL_0;           ///< 时钟相位和极性        
    Config->mDataShiftDir	= DRV_SPI_DATA_MSB;					///< 数据移位方向
    Config->mCsMode		    = DRV_SPI_CS_NOMARL;   				///< 片选方式
}

static U8 DrvGetFitBandrateDiv(const DRV_SPI_ID SpiId,U32 Bandrate)
{
    U32 BusClock;
    U8 Div = SPI_BANDRATE_MAX_DIV;
    U8 i;
    
    if(Bandrate > SPI_BANDRATE_ENUM_MAX)
    {
        BusClock = DrvGetSpiBusClock(SpiId);
        for(i=0;i <= SPI_BANDRATE_MAX_DIV;i++)
        {
            if((BusClock>>(i+1))<= Bandrate)
            {
                Div = i;
                break;
            }
        }
    }
    else
    {
    	if(DRV_SPI_ID_0 == SpiId)
    	{
    		///< SPI0: PCLK=PCLK2=120M; 分频后得到的SPI频率与期望频率存在一定的误差
    		Div = Bandrate + 1;		
    	}
    	else
    	{
    		///< SPI1&SPI2: PCLK=PCLK1=60M; 分频后得到的SPI频率与期望频率存在一定的误差
    		Div = Bandrate;			
    	}

    }
    
    if(Div > SPI_BANDRATE_MAX_DIV)
    {
        Div = SPI_BANDRATE_MAX_DIV;
    }
    else if((DRV_SPI_ID_0 == SpiId) && (Div == 0))      ///<防止SPI0分频太小  超出SPI的频率
    {
        Div = 1;	
    }
    return Div;
}
/**
* @brief	SPI设备初始化
*
* @param	SpiId			SPI编号
* @param	SpiMode			SPI工作模式，主或从
* @param	CsPinSelect		SPI片选引脚
* @param	Config			SPI配置
*
* @note		CS可以传0下来，表示不用底层操作片选
            SPI 实际输出速率  = SPI配置速率 / 1.4 
* @return	0成功，其他失败
*/
S32 DrvSpiDeviceInit(const DRV_SPI_ID SpiId, const DRV_SPI_MODE SpiMode, const U8 CsPinSelect, DRV_SPI_CONFIG *const Config)
{
    spi_parameter_struct   SpiInitParam;
    
    if(NULL == Config)
    {
        return -1;
    } 
	
	///< 使能SPI时钟
	if(DRV_SPI_ID_0 == SpiId)
	{
		rcu_periph_clock_enable(RCU_SPI0);
	}
	else if(DRV_SPI_ID_1 == SpiId)
	{
		rcu_periph_clock_enable(RCU_SPI1);
	}
	else if(DRV_SPI_ID_2 == SpiId)
	{
		rcu_periph_clock_enable(RCU_SPI2);
	}
	else
	{
		return -1;
	}
	
	memset(&(sSnbc_Dev_Spi[SpiId]), 0, sizeof(SNBC_DEV_SPI));	
	spi_i2s_deinit(sSpiBase[SpiId]);							///< SPI去初始化
	SpiInitPins(SpiId);											///< 初始化SPI引脚
	
	sSpiWorkMode[SpiId] = SpiGetMode(SpiId);	
	if(0 != sSpiWorkMode[SpiId])       							///< 双线或4线模式
    {
        SpiInitParam.trans_mode = SPI_TRANSMODE_FULLDUPLEX;
    }
    else														///< 单线传输模式
    {
        SpiInitParam.trans_mode = SPI_BIDIRECTIONAL_TRANSMIT;        
    }
	if(SpiMode == DRV_SPI_MASTER)								///< 主模式
    {
        SpiInitParam.device_mode = SPI_MASTER;
    }	
    else														///< 从模式
    {
        SpiInitParam.device_mode = SPI_SLAVE;	
    }
	
	SpiInitParam.nss 		= SPI_NSS_SOFT;						///< NSS
	SpiInitParam.frame_size = SPI_FRAMESIZE_8BIT;
	switch(Config->mClkMode)									///< CKPH和CKPL 
    {
        case DRV_SPI_CLK_PH_0_POL_0:				
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE;			
            break;
        }
        case DRV_SPI_CLK_PH_0_POL_1:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE;
            break;
        }
        case DRV_SPI_CLK_PH_1_POL_0:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_HIGH_PH_1EDGE;
            break;
        }
        case DRV_SPI_CLK_PH_1_POL_1:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_HIGH_PH_2EDGE;
            break;
        }
        default:
        {
            return -1;
        }
    }

    SpiInitParam.prescale = CTL0_PSC(DrvGetFitBandrateDiv(SpiId, Config->mClkFreq));
    
	if(Config->mDataShiftDir == DRV_SPI_DATA_MSB)	
    {
        SpiInitParam.endian = SPI_ENDIAN_MSB;		///< 高位在前
    }
    else
    {
        SpiInitParam.endian = SPI_ENDIAN_LSB;		///< 低位在前
    }
	
	spi_init(sSpiBase[SpiId], &SpiInitParam);
	
	///< SPI0且在4线模式下使能IO2/IO3输出
    if((DRV_SPI_ID_0 == SpiId) && (sSpiWorkMode[SpiId] == -2))
    {
        qspi_io23_output_enable(sSpiBase[SpiId]);
    }
	
	spi_enable(sSpiBase[SpiId]);	
	return 0;
}

/**
* @brief	SPI配置设置
*
* @param	SpiId	SPI编号
* @param	Config	SPI配置指针，包括:	mClkFreq频率的设置  
*										mClkMode模式的设置  
*										mDataShiftDir高低位设置
* @return	成功返回0，失败返回非0
*/
S32 DrvSpiConfigSet(const DRV_SPI_ID SpiId, DRV_SPI_CONFIG *const Config)
{
	spi_parameter_struct   SpiInitParam;
	U32					   SpiPeriph = sSpiBase[SpiId];
    
    if(NULL == Config)
    {
        return -1;
    } 
	
	memset(&(sSnbc_Dev_Spi[SpiId]), 0, sizeof(SNBC_DEV_SPI));	
	spi_disable(SpiPeriph);
	
	sSpiWorkMode[SpiId] = SpiGetMode(SpiId);	
	if(0 != sSpiWorkMode[SpiId])       							///< 双线或4线模式
    {
        SpiInitParam.trans_mode = SPI_TRANSMODE_FULLDUPLEX;
    }
    else														///< 单线传输模式
    {
        SpiInitParam.trans_mode = SPI_BIDIRECTIONAL_TRANSMIT;        
    }
		
	if(SPI_CTL0(SpiPeriph)&0x4)									///< 主模式
    {
        SpiInitParam.device_mode = SPI_MASTER;
    }	
    else														///< 从模式
    {
        SpiInitParam.device_mode = SPI_SLAVE;	
    }
	
	SpiInitParam.nss 		= SPI_NSS_SOFT;						///< NSS
	SpiInitParam.frame_size = SPI_FRAMESIZE_8BIT;
	switch(Config->mClkMode)									///< CKPH和CKPL 
    {
        case DRV_SPI_CLK_PH_0_POL_0:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE;			
            break;
        }
        case DRV_SPI_CLK_PH_0_POL_1:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_LOW_PH_2EDGE;
            break;
        }
        case DRV_SPI_CLK_PH_1_POL_0:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_HIGH_PH_1EDGE;
            break;
        }
        case DRV_SPI_CLK_PH_1_POL_1:
        {
			SpiInitParam.clock_polarity_phase = SPI_CK_PL_HIGH_PH_2EDGE;
            break;
        }
        default:
        {
            return -1;
        }
    }

	SpiInitParam.prescale = CTL0_PSC(DrvGetFitBandrateDiv(SpiId, Config->mClkFreq));

	if(Config->mDataShiftDir == DRV_SPI_DATA_MSB)	
    {
        SpiInitParam.endian = SPI_ENDIAN_MSB;
    }
    else
    {
        SpiInitParam.endian = SPI_ENDIAN_LSB;
    }
	
	spi_init(sSpiBase[SpiId], &SpiInitParam);
	spi_enable(sSpiBase[SpiId]);
	
    return 0;
}

/**
* @brief	SPI阻塞传输
* @param	SpiId		SPI编号
* @param	SpiCs		SPI片选选择
* @param	CsHoldEn	SPI片选控制使能						
* @param	SendBuf		发送缓冲区，只接收时可为NULL
* @param	RecvBuf		接收缓冲区，只发送时可为NULL
* @param	Length		传输长度
* @return	成功返回0，失败返回非0
*/ 
S32 DrvSpiBlockTransfer(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, const DRV_SPI_HOLD CsHoldEn, U8 *const SendBuf, U8 *const RecvBuf, U32 Length)
{
    U32 i;

    if((SendBuf == NULL) && (RecvBuf == NULL))
    {
        return -1;
    }

    if(DRV_SPI_HOLD_ACTIVE & CsHoldEn)											///< 拉低片选线, 选通
    {
        DrvSpiCsSet(SpiId, SpiCs, DRV_SPI_HOLD_ACTIVE);
    }
	
	while(spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_RBNE) != RESET)
	{
		spi_i2s_data_receive(sSpiBase[SpiId]);
	}

    i = 0;
    if(SendBuf != NULL && (RecvBuf == NULL))									///< 阻塞发送
    {   
        while (i < Length)
        {
            while(RESET == spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_TBE));
            spi_i2s_data_transmit(sSpiBase[SpiId], SendBuf[i++]);
            if(sSpiWorkMode[SpiId] == -1)										///< 双工模式
            {
                while (spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_RBNE) == RESET);
                spi_i2s_data_receive(sSpiBase[SpiId]);
            }
        }
        while(SET == spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_TRANS));
    }
	
    i = 0;
    if(RecvBuf != NULL )						
    { 
        if(SendBuf != NULL)														///< 阻塞发送并接收
        {
            while (i < Length)
            {
                while (spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_TBE) == RESET);
                spi_i2s_data_transmit(sSpiBase[SpiId], SendBuf[i]);

                while (spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_RBNE) == RESET);
                RecvBuf[i++] = spi_i2s_data_receive(sSpiBase[SpiId]);
            }
        }
        else
        {
            while (i < Length)													///< 阻塞接收
            {
                while (spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_TBE) == RESET);
                spi_i2s_data_transmit(sSpiBase[SpiId], 0xff);

                while (spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_RBNE) == RESET);
                RecvBuf[i++] = spi_i2s_data_receive(sSpiBase[SpiId]);
            }
        }
    } 
	
    if(DRV_SPI_HOLD_CLR & CsHoldEn)												///<  拉高片选线, 禁止片选
    {
        DrvSpiCsSet(SpiId, SpiCs, DRV_SPI_HOLD_CLR);
    }	
	
    return 0;
}

/**
* @brief	SPI中断传输
* @param	SpiId		SPI编号
* @param	SpiCs		SPI片选选择
* @param	CsHoldEn	SPI片选控制使能						
* @param	SendBuf		发送缓冲区，只接收时可为NULL
* @param	SendBuf		接收缓冲区，只发送时可为NULL
* @param	Length		传输长度
* @return	成功返回0，失败返回非0
*/
S32 DrvSpiIntTransfer(const DRV_SPI_ID SpiId,const DRV_SPI_CS SpiCs,const DRV_SPI_HOLD CsHoldEn,U8 *const SendBuf,U8 *const RecvBuf, U32 Length)
{
	while(spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_RBNE) != RESET)
	{
		spi_i2s_data_receive(sSpiBase[SpiId]);			///< 清RNE标记
	}	
	
    sSnbc_Dev_Spi[SpiId].mLen		= Length;
    sSnbc_Dev_Spi[SpiId].mSendBuf	= SendBuf;
    sSnbc_Dev_Spi[SpiId].mRcvBuf    = RecvBuf;
    sSnbc_Dev_Spi[SpiId].mCurIndex	= 0;
	
	
	if((SendBuf != NULL) && (RecvBuf == NULL))			///< 只发送不接收
	{
		spi_i2s_interrupt_enable(sSpiBase[SpiId], SPI_I2S_INT_TBE);
	}
	else												///< 单接收(发送0xff)/发送且接收
	{
		spi_i2s_interrupt_enable(sSpiBase[SpiId], SPI_I2S_INT_TBE);
		spi_i2s_interrupt_enable(sSpiBase[SpiId], SPI_I2S_INT_RBNE);
	}

    return 0;
}

/**
* @brief	设置中断回调函数
* @param	SpiPeriph  	  SPI外设基地址
* @param	SpiId   	  SPI编号
* @note		无
* @return	无
*/
static void DrvSpiIntCallBack(U32 SpiPeriph, U8 SpiId)
{
    if(spi_i2s_interrupt_flag_get(SpiPeriph, SPI_FLAG_RBNE) != RESET)
    {
        if(sSnbc_Dev_Spi[SpiId].mRcvBuf != NULL)
        {
            sSnbc_Dev_Spi[SpiId].mRcvBuf[sSnbc_Dev_Spi[SpiId].mCurIndex] = spi_i2s_data_receive(SpiPeriph);
            sSnbc_Dev_Spi[SpiId].mCurIndex++;
            if(sSnbc_Dev_Spi[SpiId].mCurIndex < sSnbc_Dev_Spi[SpiId].mLen)
            {
                if(sSnbc_Dev_Spi[SpiId].mSendBuf == NULL)
                {
                    if(Spi_sSpiIntCallBack[SpiId] != NULL)
                    {
                        Spi_sSpiIntCallBack[SpiId](NULL, DRV_SPI_INT_RX);
                    }
                }
            }
            else																///< 接收完成
            {
				DrvSpiIntEnSet((DRV_SPI_ID)SpiId, DRV_SPI_INT_RX, DISABLE);
                if(Spi_sSpiIntCallBack[SpiId] != NULL)
                {
                    Spi_sSpiIntCallBack[SpiId](NULL, DRV_SPI_INT_TRANS_COMPLETE); 
                }                
            }
        }   
    }

    if(spi_i2s_interrupt_flag_get(SpiPeriph, SPI_I2S_INT_FLAG_TBE) != RESET)
    {
        if(sSnbc_Dev_Spi[SpiId].mSendBuf != NULL)
        {            
			spi_i2s_data_transmit(SpiPeriph, sSnbc_Dev_Spi[SpiId].mSendBuf[sSnbc_Dev_Spi[SpiId].mCurIndex]);
            if(sSnbc_Dev_Spi[SpiId].mRcvBuf == NULL)
            {
                sSnbc_Dev_Spi[SpiId].mCurIndex++;				
                if(sSnbc_Dev_Spi[SpiId].mCurIndex == sSnbc_Dev_Spi[SpiId].mLen) ///< 发送完成
                {
					DrvSpiIntEnSet((DRV_SPI_ID)SpiId, DRV_SPI_INT_TX, DISABLE);
                    if(Spi_sSpiIntCallBack[SpiId] != NULL)
                    {
                        Spi_sSpiIntCallBack[SpiId](NULL, DRV_SPI_INT_TRANS_COMPLETE);                        
                    }   
                }
                else	
                {
                    if(Spi_sSpiIntCallBack[SpiId] != NULL)  
                    {
                        Spi_sSpiIntCallBack[SpiId](NULL, DRV_SPI_INT_TX);
                    }
                }
            }
        }  
        else if(sSnbc_Dev_Spi[SpiId].mRcvBuf != NULL && sSnbc_Dev_Spi[SpiId].mCurIndex < sSnbc_Dev_Spi[SpiId].mLen)
        {
            spi_i2s_data_transmit(SpiPeriph, 0xff);
        }
        else
        {
            spi_i2s_interrupt_disable(sSpiBase[SpiId], SPI_I2S_INT_TBE);
        }
    }	
}

/**
* @brief	Spi0中断入口函数
* @return	无
*/
void SPI0_IRQHandler(void)
{
	DRVIER_INT_ENTER();
	DrvSpiIntCallBack(SPI0, 0); 
	DRVIER_INT_EXIT();	
}
/**
* @brief	Spi1中断入口函数
* @return	无
*/
void SPI1_IRQHandler(void)
{   
	DRVIER_INT_ENTER();
    DrvSpiIntCallBack(SPI1, 1);       
	DRVIER_INT_EXIT();	
}
/**
* @brief	Spi2中断入口函数
* @return	无
*/
void SPI2_IRQHandler(void)
{
	DRVIER_INT_ENTER();
    DrvSpiIntCallBack(SPI2, 2);      
	DRVIER_INT_EXIT();	
}

/**
* @brief	Spi中断回调函数注册
* @param	SpiId		SPI编号
* @param	IntCallBack	中断回调函数
* @return	成功返回0，失败返回非0
*/
S32 DrvSpiIntCallBackSet(const DRV_SPI_ID SpiId,void (*IntCallBack)(U32 TransLen,DRV_SPI_INT_TYPE IntType))
{
    SET_FUNC_POINTER(Spi_sSpiIntCallBack[SpiId], IntCallBack);

    return 0;
}

/**
* @brief	Spi中断使能设置
* @param	SpiId		SPI编号
* @param	Mask		中断掩码 @ref SPI_INT_TYPE
* @param	Enable		使能/禁止，使用宏ENABLE,DISABLE
* @return	0成功，其他失败
*/
S32 DrvSpiIntEnSet(const DRV_SPI_ID SpiId, const U8 Mask, const U8 Enable)
{
    IRQn_Type        IntType;
	
	spi_disable(sSpiBase[SpiId]);
	
	if(SpiId == DRV_SPI_ID_0)
    {
        IntType = SPI0_IRQn;
    }
    else if(SpiId == DRV_SPI_ID_1)
    {
        IntType = SPI1_IRQn;
    }
    else if(SpiId == DRV_SPI_ID_2)
    {
        IntType = SPI2_IRQn;
    }
	else
	{
		return -1;
	}
	
	NVIC_EnableIRQ(IntType);					///< 为了不影响模块内其余的中断状态, 因此NVIC中该模块的中断一直使能
		
	if(Mask & DRV_SPI_INT_RX)					///< 接收中断
    {
		if(Enable != STD_DISABLE)
		{
			spi_i2s_interrupt_enable(sSpiBase[SpiId], SPI_I2S_INT_RBNE);
		}
		else
		{
			spi_i2s_interrupt_disable(sSpiBase[SpiId], SPI_I2S_INT_RBNE);
		}
    } 
	
    if(Mask & DRV_SPI_INT_TX)					///< 传输中断
    {
		if(Enable != STD_DISABLE)
		{
			spi_i2s_interrupt_enable(sSpiBase[SpiId], SPI_I2S_INT_TBE);
		}
		else
		{
			spi_i2s_interrupt_disable(sSpiBase[SpiId], SPI_I2S_INT_TBE);
		}
    }
	
	spi_enable(sSpiBase[SpiId]);
	    
    return 0;
}


/**
* @brief	Spi DMA模式配置
*
* @param	SpiId		SPI索引
* @param	pDmaConfig	DMA的配置
* @param	DmaTxRx		DMA传输或接收， 0--发送， 1--接收
*
* @return	0成功，其他失败
*/
static void DrvSpiDmaConfig(DRV_SPI_ID SpiId, dma_parameter_struct *pDmaConfig, U8 DmaTxRx)
{	
	U32 SpiPeriph = sSpiBase[SpiId];
	U32 DmaPeriph = sSpiDmaResource[SpiId][DmaTxRx].mPeriph;
	dma_channel_enum DmaChn = sSpiDmaResource[SpiId][DmaTxRx].mCh;
	
	spi_disable(SpiPeriph);	
    dma_interrupt_disable(DmaPeriph, DmaChn, DMA_INT_FTF);			///< 禁止传输完成中断
	
    spi_dma_disable(SpiPeriph, SPI_DMA_TRANSMIT);
	spi_dma_disable(SpiPeriph, SPI_DMA_RECEIVE);
	
	dma_channel_disable(DmaPeriph, DmaChn);
	
    dma_deinit(DmaPeriph, DmaChn); 	

    pDmaConfig->periph_inc      = DMA_PERIPH_INCREASE_DISABLE;		///< 外设地址增长禁止
    pDmaConfig->periph_width 	= DMA_PERIPHERAL_WIDTH_8BIT;		///< 数据传输尺寸为8位
    pDmaConfig->memory_width 	= DMA_MEMORY_WIDTH_8BIT;			///< 内存增长宽度
    pDmaConfig->priority     	= DMA_PRIORITY_MEDIUM;				///< DMA通道优先级
    
    if(DmaTxRx == 1)    pDmaConfig->priority     	= DMA_PRIORITY_ULTRA_HIGH;				///< DMA通道优先级
	
	dma_init(DmaPeriph, DmaChn, *pDmaConfig);
	dma_circulation_disable(DmaPeriph, DmaChn);						///< 周期模式禁止
    dma_memory_to_memory_disable(DmaPeriph, DmaChn);				///< 禁止内存到内存
	
    NVIC_EnableIRQ(sSpiDmaResource[SpiId][DmaTxRx].mIrq);   
    
    dma_interrupt_enable(DmaPeriph, DmaChn, DMA_INT_FTF);			///< 使能传输完成中断
	dma_channel_enable(DmaPeriph, DmaChn);		
}

/**
* @brief	Spi DMA模式数据传输
*
* @param	SpiId		SPI编号
* @param	SpiCs		SPI的片选
* @param	CsHoldEn	SPI的片选保持是否使能
* @param	SendBuf		发送缓冲
* @param	RecvBuf		接受缓冲
* @param	Length		传输数据的长度
* @note		在发送时, SendBuf不能为NULL; RecvBuf可为NULL
			在接收时, SendBuf和RecvBuf都不可为NULL
* @return	0成功，其他失败
*/
S32 DrvSpiDmaTransfer(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, const DRV_SPI_HOLD CsHoldEn, U8 *const SendBuf, U8 *const RecvBuf, U32 Length)
{
	dma_parameter_struct	tempDma;
	
    if(SendBuf == NULL && RecvBuf == NULL)
    {
        return -1;
    }
	
	if( DRV_SPI_ID_2 == SpiId)				
	{
		rcu_periph_clock_enable(RCU_DMA1);							///< 使能DMA1时钟
	}
	else
	{
		rcu_periph_clock_enable(RCU_DMA0);							///< 使能DMA0时钟
	}

	while(spi_i2s_flag_get(sSpiBase[SpiId], SPI_FLAG_RBNE) != RESET)
	{
		spi_i2s_data_receive(sSpiBase[SpiId]);
	}
	
	if((SendBuf!= NULL) && (RecvBuf == NULL ))
    {		
        tempDma.memory_addr	= (U32)SendBuf;							///< 内存地址
        tempDma.memory_inc	= DMA_MEMORY_INCREASE_ENABLE;       	///< 内存地址增长使能
        tempDma.direction	= DMA_MEMORY_TO_PERIPHERAL;				///< 内存到外设
        tempDma.periph_addr	= (uint32_t)&SPI_DATA(sSpiBase[SpiId]); ///< 外设地址   
        tempDma.number		= Length;       						///< DMA数据传输长度
        DrvSpiDmaConfig(SpiId, &tempDma, 0);
        
        spi_dma_enable(sSpiBase[SpiId], SPI_DMA_TRANSMIT);		
        sSpiDma_RcvLen[SpiId] = Length;
    }

    if(RecvBuf != NULL)
    {   
        U32 DmaPeriph;
        dma_channel_enum DmaChn;

        ///< SPI接收配置
        tempDma.memory_addr	= (U32)RecvBuf;							///< 接收数据内存地址
        tempDma.memory_inc	= DMA_MEMORY_INCREASE_ENABLE;       	///< 内存地址增长使能
        tempDma.direction	= DMA_PERIPHERAL_TO_MEMORY;				///< 外设到内存
        tempDma.periph_addr	= (uint32_t)&SPI_DATA(sSpiBase[SpiId]); ///< 外设地址          
        tempDma.number		= Length;       						    ///< 接收长度
        DrvSpiDmaConfig(SpiId, &tempDma, 1);

		///< 需要先发送命令地址等相关信息， 才能读取从设备的数据
		tempDma.memory_addr	= (U32)SendBuf;							///< 发送数据内存地址
        tempDma.memory_inc	= DMA_MEMORY_INCREASE_DISABLE;          ///< 内存地址增长禁止
        tempDma.direction	= DMA_MEMORY_TO_PERIPHERAL;				///< 内存到外设
        tempDma.periph_addr	= (uint32_t)&SPI_DATA(sSpiBase[SpiId]); ///< 外设地址       
        tempDma.number		= Length;       						    ///< 发送长度
        DrvSpiDmaConfig(SpiId, &tempDma, 0);

        DmaPeriph = sSpiDmaResource[SpiId][0].mPeriph;
        DmaChn = sSpiDmaResource[SpiId][0].mCh;
        dma_channel_disable(DmaPeriph, DmaChn);
        dma_interrupt_disable(DmaPeriph, DmaChn, DMA_INT_FTF); ///< 关闭传输完成中断
        dma_channel_enable(DmaPeriph, DmaChn);

        spi_dma_enable(sSpiBase[SpiId], SPI_DMA_RECEIVE);
        spi_dma_enable(sSpiBase[SpiId], SPI_DMA_TRANSMIT);

        sSpiDma_RcvLen[SpiId] = Length;
    }

    spi_enable(sSpiBase[SpiId]);	
	
    return 0;
}

/**
* @brief	设置DMA传输完成后中断回调函数
* @param	SpiId					SPI编号
* @param	DmaCompleteCallBack		传输完成中断回调函数指针
* @return	0成功，其他失败
*/
S32 DrvSpiDmaCompleteIntCallBackSet(const DRV_SPI_ID SpiId, void *DmaCompleteCallBack)
{	
	Dma_SetCallBack(sSpiDmaResource[SpiId][0].mDmaInx, 
					(U8)(sSpiDmaResource[SpiId][0].mCh), DmaCompleteCallBack);
    return 0;
}

/**
* @brief	设置DMA传输完成后中断回调函数
* @param	SpiId					SPI编号
* @param	DmaCompleteCallBack		传输完成中断回调函数指针
* @return	0成功，其他失败
*/
S32 DrvSpiDmaRecvCompleteIntCallBackSet(const DRV_SPI_ID SpiId, void *DmaCompleteCallBack)
{
    Dma_SetCallBack(sSpiDmaResource[SpiId][1].mDmaInx, 
					(U8)(sSpiDmaResource[SpiId][1].mCh), DmaCompleteCallBack);
    return 0;
}

/**
* @brief	获取DMA已传输长度
*
* @param	SpiId         SPI编号
*
* @return	DMA传输长度
*/
U32 DrvGetSpiDmaTransLen(const DRV_SPI_ID SpiId)
{
    return (sSpiDma_RcvLen[SpiId] - 
			dma_transfer_number_get(sSpiDmaResource[SpiId][0].mPeriph, sSpiDmaResource[SpiId][0].mCh));
}

/**
* @brief	Spi DMA中断使能
*
* @param	SpiId		SPI编号
* @param	Enable		使能/禁止，使用宏ENABLE,DISABLE
* @return	0成功，其他失败
*/
S32 DrvSpiDmaIntEnSet(const DRV_SPI_ID SpiId, const U8 Enable)
{	
    if(Enable)
    {
        dma_interrupt_enable(sSpiDmaResource[SpiId][0].mPeriph, 
							 sSpiDmaResource[SpiId][0].mCh, DMA_INT_FTF);	///< 使能传输完成中断
    }
    else
    {
        dma_interrupt_disable(sSpiDmaResource[SpiId][0].mPeriph, 
							  sSpiDmaResource[SpiId][0].mCh, DMA_INT_FTF);	///< 禁止传输完成中断
    }

    return 0;
}

/**
* @brief	片选操作
*
* @param	SpiId		SPI编号
* @param	SpiCs		选择从设备 ，有多个从设备时 ，通过该参数选择  
* @param	CsHoldEn	0:拉低，非0:拉高  
* @return	0成功，其他失败
*/
U8 DrvSpiCsSet(const DRV_SPI_ID SpiId, const DRV_SPI_CS SpiCs, int CsHoldEn)
{
	if(CsHoldEn == DRV_SPI_HOLD_ACTIVE)
	{
		DrvSpiSetNSS(SpiId, 0);		///< 拉低片选
	}
	else
	{
		DrvSpiSetNSS(SpiId, 1); 	///< 拉高片选
	}

	return 0;
}


/**
* @brief	SPI Qmode模式下读写设置
*
* @param	SpiId		SPI编号
* @param	RW		    1-使能写， 0-使能读
* @return	0成功，其他失败
*/
S32 DrvSpiQModeRwSet(const DRV_SPI_ID SpiId,  U32 RW)
{
	U32 SpiPeriph = sSpiBase[SpiId];

	if(RW)
	{
		qspi_write_enable(SpiPeriph);
	}
	else
	{
		qspi_read_enable(SpiPeriph);
	}

	return 0;
}

/**
* @brief	SPI Qmode使能设置
*
* @param	SpiId		SPI编号
* @param	Enable		使能或禁止, STD_ENABLE or STD_DISABLE
* @return	0成功，其他失败
*/
S32 DrvSpiQModeEnSet(const DRV_SPI_ID SpiId,  U32 Enable)
{
	U32 SpiPeriph = sSpiBase[SpiId];

	if(Enable)
	{
		qspi_enable(SpiPeriph);
	}
	else
	{
		qspi_disable(SpiPeriph);
	}

	return 0;
}
